<?php

function setStatusBaru($status,$statusrevisi){
    if($status==0 && $statusrevisi==0){
        return "<div style='margin-top: 10px;text-align: center'><span class='label label-primary' style='font-size: 14px'>Baru</span></div>";
    }
    elseif($status==1&& $statusrevisi==0){
        return "<div style='margin-top: 10px;text-align: center'><span class='label label-primary' style='font-size: 14px'>Realisasi</span></div>";
    }
    elseif($status==2&& $statusrevisi==0){
        return "<div style='margin-top: 10px;text-align: center'><span class='label label-success' style='font-size: 14px'>Verifikasi</span></div>";
    }
    elseif($status==3&& $statusrevisi==0){
        return "<div style='margin-top: 10px;text-align: center'><span class='label label-success' style='font-size: 14px'>Disposisi Supervisor</span></div>";
    }
    elseif($status==4&& $statusrevisi==0){
        return "<div style='margin-top: 10px;text-align: center'><span class='label label-success' style='font-size: 14px'>Pending Disposisi</span></div>";
    }
    elseif($status==5&& $statusrevisi==0){
        return "<div style='margin-top: 10px;text-align: center'><span class='label label-warning' style='font-size: 14px'>Proses</span></div>";
    }

}

function setStatusRevisi($statusrevisi,$statusbaru)
{
    if ($statusrevisi == 1 && $statusbaru==0) {
        return "<div style='margin-top: 10px;text-align: center'><span class='label label-danger' style='font-size: 14px'>Revisi</span></div>";
    }elseif ($statusrevisi==3&& $statusbaru==0){
        return "<div style='margin-top: 10px;text-align: center'><span class='label label-primary' style='font-size: 14px'>Realisasi</span></div>";
    }
    elseif ($statusrevisi==2 && $statusbaru==0){
        return "<div style='margin-top: 10px;text-align: center'><span class='label label-warning-light' style='font-size: 14px'>Proses</span></div>";

    }
}