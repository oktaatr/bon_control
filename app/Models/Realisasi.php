<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Realisasi extends Model
{
    protected $table='realisasi';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getDatabon(){
        return $this->hasOne('App\Models\Bonc','id','bonc_id');
    }
}
