<?php

namespace App\Models;
use App\Models\Pelanggan;
use Illuminate\Database\Eloquent\Model;

class PemakaianAir extends Model
{
    protected $table='pemakaian_air';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getBonc(){
        return $this->hasOne('App\Models\Bonc','id','idbonc');
    }
    public function getPelanggan(){
        return $this->hasOne('App\Models\Pelanggan','id','idpel');
    }
}
