<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bonc extends Model
{
    protected $table = 'bonc';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getPemakaianAir(){
        return $this->hasMany('App\Models\PemakaianAir','idbonc','id');
    }
    public function getPelanggan(){
        return $this->hasOne('App\Models\Pelanggan','id','id_pel');
    }
    public function getPetugas(){
        return $this->hasOne('App\Models\User','id','id_user');
    }
    public function getBonc(){
        return $this->hasOne('App\Models\Realisasi','bonc_id','id');
    }
}
