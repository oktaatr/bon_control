<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $table='pelanggan';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getAir(){
        return $this->hasMany('App\Models\PemakaianAir','idpel','id');
    }
}
