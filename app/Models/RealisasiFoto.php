<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RealisasiFoto extends Model
{
    protected $table='foto_realisasi';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getRealisasi(){
        return $this->hasOne('App\Models\Realisasi','id','id_realisasi');
    }
}
