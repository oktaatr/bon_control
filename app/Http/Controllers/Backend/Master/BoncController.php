<?php
/**
 * Created by PhpStorm.
 * User: Okta Athour Rizqi
 * Date: 30/05/2018
 * Time: 12:18
 */

namespace App\Http\Controllers\Backend\Master;

use App\Models\Pelanggan;
use DB;
use App\Models\PemakaianAir;
use App\Models\Realisasi;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bonc;

class BoncController extends Controller
{
    public  function index(){

        $data=Bonc::all();
        $params=[
            'data'=>$data,
            'title'=>'Bon Control'
        ];

        return view('backend.master.bonc.index',$params);

    }
    public function detail(Request $request){
        $boncId=$request->get('id');
        $dataBonc=Bonc::where(['id'=>$boncId])->first();
        $TglBonc=new Carbon($dataBonc->tglbonc);
        //$getLast=$TglBonc->subMonths(6)->toDateString();
        //$getAir=date('M', strtotime($getLast));
        //$tglBonc=date('m', strtotime($dataBonc->tglbonc));
//        $dataPel=Pelanggan::where(['id'=>$dataBonc->id_pel])->first();
//        $datapemair=PemakaianAir::where('tglcatat',date('m', strtotime($dataBonc->tglbonc)))->orderBy('tglcatat','DESC')->get();
//        $q->where(DB::raw("DATE(created_at) = '".date('Y-m-d')."'"));
//        $datapemair=DB::table("pemakaian_air")
//            ->whereMonth('tglcatat', '<=', '06')
//            ->get();
        $dataAir=PemakaianAir::where('idpel',$dataBonc->id_pel)->get();
        foreach ($dataAir as $item){
            $idpel[]=$item->id;
        }
        $datapemair=PemakaianAir::whereIn('idpel',[$dataBonc->id_pel])->where("tglcatat",">=", $TglBonc->subMonths(7))->get();
        $params=[
            'title' =>'Bon Control dengan nomor'.$dataBonc->no_bonc,
            'dataBonc'=>$dataBonc,
            'datapemair'=>$datapemair
        ];

        return view('backend.master.bonc.detail',$params);
    }
    public  function form(Request $request){

        $id = $request->input('id');
        if($id){
            $data = Bonc::find($id);
        }else{
            $data = new Bonc();
        }
        $userOption=User::all();
        $pelangganOption=Pelanggan::all();
        $params = [
            'title' => 'Bon Control',
            'data' => $data,
            'userOption'=>$userOption,
            'pelangganOption'=>$pelangganOption
        ];
        return view('backend.master.bonc.form',$params);
    }
    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        $status=new Realisasi();
        if($id){
            $data = Bonc::find($id);
        }else{
            $data = new Bonc();

        }
        $data->no_bonc = $request->no_bonc;
        $data->tglbonc = $request->tglbonc;
        $data->bagpemair = $request->bagpemair;
        $data->sbrdt = $request->sbrdt;
        $data->asal_adu=$request->asal_adu;
        $data->tlp=$request->tlp;
        $data->masalah=$request->masalah;
        $data->id_pel=$request->id_pel;
        $data->id_user=$request->id_user;
        $status->status_baru=0;
        $status->status_revisi=0;
        try{
            $data->save();
            $status->bonc_id=$data->id;
            $status->save();
            return "
            <div class='alert alert-success'>Peran berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Peran gagal disimpan!</div>";
        }

    }
    public  function  delete(Request $request){

        $id = intval($request->input('id', 0));
        try{
            Bonc::find($id)->delete();
            return "
            <div class='alert alert-success'>Peran berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Peran gagal dihapus!</div>";
        }

    }
}