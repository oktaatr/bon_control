<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 07/05/18
 * Time: 20.47
 */

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\Pelanggan;
use Illuminate\Http\Request;
class PelangganController extends Controller
{
    public  function index(){

        $data=Pelanggan::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Pengguna'
        ];

        return view('backend.master.pelanggan.index',$params);

    }

    public  function form(Request $request){

        $id = $request->input('id');
        if($id){
            $data = Pelanggan::find($id);
        }else{
            $data = new Pelanggan();
        }
        $params = [
            'title' => 'Manajemen Pengguna',
            'data' => $data,
        ];
        return view('backend.master.pelanggan.form',$params);
    }
    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Pelanggan::find($id);
        }else{
            $data = new Pelanggan();
        }
        $data->nopel=$request->nopel;
        $data->zone=$request->zone;
        $data->retrib=$request->retrib;
        $data->nopa=$request->nopa;
        $data->kdtrp=$request->kdtrp;
        $data->dtmtr=$request->dtmtr;
        $data->nama_pel = $request->nama_pel;
        $data->alamat = $request->alamat;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Pelanggan berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Pelanggan gagal disimpan!</div>";
        }

    }
    public  function  delete(Request $request){

        $id = intval($request->input('id', 0));
        try{
            Pelanggan::find($id)->delete();
            return "
            <div class='alert alert-success'>Pelanggan berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Pelanggan gagal dihapus!</div>";
        }

    }

}