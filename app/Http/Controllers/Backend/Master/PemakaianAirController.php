<?php

namespace App\Http\Controllers\Backend\Master;

use App\Models\Bonc;
use App\Models\PemakaianAir;
use Illuminate\Http\Request;
use App\Models\Pelanggan;
use App\Http\Controllers\Controller;

class PemakaianAirController extends Controller
{
    public  function index(){

        $data=PemakaianAir::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Pengguna'
        ];

        return view('backend.master.pemakaian-air.index',$params);

    }
    public  function form(Request $request){

        $id = $request->input('id');
        if($id){
            $data = PemakaianAir::find($id);
        }else{
            $data = new PemakaianAir();
        }
        $pelOption=Pelanggan::all();
        $bonOption=Bonc::all();
        $params = [
            'title' => 'Air Pelanggan',
            'data' => $data,
            'pelOption'=>$pelOption
        ];
        return view('backend.master.pemakaian-air.form',$params);
    }
    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = PemakaianAir::find($id);
        }else{
            $data = new PemakaianAir();

        }
        $data->tglcatat = $request->tglcatat;
        $data->mtlalu = $request->mtlalu;
        $data->mtkini = $request->mtkini;
        $data->mair = $data->mtkini - $data->mtlalu;
        $data->idpel=$request->idpel;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Peran berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Peran gagal disimpan!</div>";
        }

    }
    public  function  delete(Request $request){

        $id = intval($request->input('id', 0));
        try{
            PemakaianAir::find($id)->delete();
            return "
            <div class='alert alert-success'>Peran berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Peran gagal dihapus!</div>";
        }

    }
}
