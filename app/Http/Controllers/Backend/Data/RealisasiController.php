<?php

namespace App\Http\Controllers\Backend\Data;

use App\Models\Bonc;
use App\Models\Realisasi;
use Illuminate\Http\Request;
use App\Models\PemakaianAir;
use DB;
use App\Http\Controllers\Controller;

class RealisasiController extends Controller
{
    public  function index(){

        $data=Realisasi::all();
        $params=[
            'data'=>$data,
            'title'=>'Status Realisasi'
        ];

        return view('backend.data.realisasi.index',$params);

    }
    public function detail(Request $request){
        $RealisasiId=$request->get('id');
        $dataRealisasi=Realisasi::where(['id'=>$RealisasiId])->first();
        $dataBonc=Bonc::where(['id'=>$dataRealisasi->bonc_id])->first();
//        $datapemair=PemakaianAir::where(['idbonc'=>$dataBonc->id])->orderBy('tglcatat','ASC')->get();
        $params=[
            'title' =>'Bon Control dengan nomor',
            'dataRealisasi'=>$dataRealisasi,
            'dataBonc'=>$dataBonc,
//            'datapemair'=>$datapemair
        ];

        return view('backend.data.realisasi.detail',$params);
    }
    public  function formRevisi(Request $request){

        $id = $request->input('id');
        $data = Realisasi::find($id);
        $params = [
            'data' => $data,
        ];
        return view('backend.data.realisasi.formrevisi',$params);
    }
    public  function formVerif(Request $request){

        $id = $request->input('id');
        $data = Realisasi::find($id);
        $params = [
            'data' => $data,
        ];
        return view('backend.data.realisasi.formverif',$params);
    }
    public  function  saveRevisi(Request $request){
        $id = $request->input('id');
        $data = Realisasi::find($id);
        $data->ket_realisasi = $request->ket_revisi;
        $data->status_revisi = 1;
        $data->status_baru = 0;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Data berhasil direvisi</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Revisi gagal!</div>";
        }

    }
    public  function  saveSupervisor(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Realisasi::find($id);
        }
        $data->ket_realisasi = $request->ket_supervisor;
        $data->status_revisi = 0;
        $data->status_baru = 3;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Data berhasil di verifikasi oleh supervisor</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Verifikasi supervisor gagal!</div>";
        }

    }
    public  function  delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            Realisasi::find($id)->delete();
            return "
            <div class='alert alert-success'>Peran berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Peran gagal dihapus!</div>";
        }

    }

    public function verifikasi(Request $request){
        $id = $request->input('id');
        $data=Realisasi::find($id);
        $data->status_baru=$request->status_baru;
        $data->status_revisi=0;
        $data->ket_realisasi=null;
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Verifikasi berhasil</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Peran gagal dihapus!</div>";
        }
    }

    public function filterRealisasi(Request $request)
    {
        $filterStatus=$request->get('status');
        $columns = array(
            0 =>'id',
            1 =>'status_baru',
            2=> 'tgl_realisasi',
            3=> 'bonc_id',
            4=> 'nama_petugas',
            5=> 'nopel',
            6=> 'pelanggan',
            7=> 'aksi',
        );

        $totalData = Realisasi::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))) {
            if (empty($filterStatus)||$filterStatus == 'all') {
                $posts = Realisasi::select('realisasi.id',
                    'realisasi.status_baru', 'realisasi.status_revisi', 'realisasi.tgl_realisasi',
                    'bonc.no_bonc', 'user.nama as nama_petugas', 'pelanggan.nopel', 'pelanggan.nama_pel')
                    ->join('bonc', 'bonc.id', '=', 'bonc_id')
                    ->join('pelanggan', 'pelanggan.id', '=', 'bonc.id_pel')
                    ->join('user', 'user.id', '=', 'bonc.id_user')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();

                $totalFiltered = Realisasi::select('realisasi.id',
                    'realisasi.status_baru', 'realisasi.status_revisi', 'realisasi.tgl_realisasi',
                    'bonc.no_bonc', 'user.nama as nama_petugas', 'pelanggan.nopel', 'pelanggan.nama_pel')
                    ->join('bonc', 'bonc.id', '=', 'bonc_id')
                    ->join('pelanggan', 'pelanggan.id', '=', 'bonc.id_pel')
                    ->join('user', 'user.id', '=', 'bonc.id_user')
                    ->count();
            }
            elseif ($filterStatus=='new'){
                $posts =  Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','pelanggan.nopel','pelanggan.nama_pel','user.nama as nama_petugas')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->where(function ($query) {
                        $query->where('realisasi.status_baru',0)
                            ->where('realisasi.status_revisi',0);
                    })
                    //                ->whereRaw("realisasi.tgl_realisasi:date LIKE '%$search%'")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

                $totalFiltered = Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','user.nama as nama_petugas','pelanggan.nopel','pelanggan.nama_pel')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->where(function ($query) {
                        $query->where('realisasi.status_baru',0)
                            ->where('realisasi.status_revisi',0);
                    })
//                ->where('realisasi.tgl_realisasi::date', 'LIKE',"%{$search}%")
                    ->count();
            }
            elseif ($filterStatus<=5){
                $posts =  Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','pelanggan.nopel','pelanggan.nama_pel','user.nama as nama_petugas')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->where('realisasi.status_baru',$filterStatus)
                    //                ->whereRaw("realisasi.tgl_realisasi:date LIKE '%$search%'")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

                $totalFiltered = Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','user.nama as nama_petugas','pelanggan.nopel','pelanggan.nama_pel')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->where('realisasi.status_baru',$filterStatus)
//                ->where('realisasi.tgl_realisasi::date', 'LIKE',"%{$search}%")
                    ->count();

            }
            elseif ($filterStatus==6){
                $posts =  Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','pelanggan.nopel','pelanggan.nama_pel','user.nama as nama_petugas')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->where('realisasi.status_revisi',1)
                    //                ->whereRaw("realisasi.tgl_realisasi:date LIKE '%$search%'")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

                $totalFiltered = Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','user.nama as nama_petugas','pelanggan.nopel','pelanggan.nama_pel')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->where('realisasi.status_revisi',1)
//                ->where('realisasi.tgl_realisasi::date', 'LIKE',"%{$search}%")
                    ->count();
            }
            elseif($filterStatus==7){
                $posts =  Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','pelanggan.nopel','pelanggan.nama_pel','user.nama as nama_petugas')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->where('realisasi.status_revisi',2)
                    //                ->whereRaw("realisasi.tgl_realisasi:date LIKE '%$search%'")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

                $totalFiltered = Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','user.nama as nama_petugas','pelanggan.nopel','pelanggan.nama_pel')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->where('realisasi.status_revisi',2)
//                ->where('realisasi.tgl_realisasi::date', 'LIKE',"%{$search}%")
                    ->count();
            }
            elseif ($filterStatus==8){
                $posts =  Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','pelanggan.nopel','pelanggan.nama_pel','user.nama as nama_petugas')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->where('realisasi.status_revisi',3)
                    //                ->whereRaw("realisasi.tgl_realisasi:date LIKE '%$search%'")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

                $totalFiltered = Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','user.nama as nama_petugas','pelanggan.nopel','pelanggan.nama_pel')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->where('realisasi.status_revisi',3)
//                ->where('realisasi.tgl_realisasi::date', 'LIKE',"%{$search}%")
                    ->count();
            }
        }

        //cek search disini
        else {
            $search = strtoupper($request->input('search.value'));
            $search2=$request->input('search.value');
            $search2=date('d-m-Y',strtotime($search2));
            if(empty($filterStatus)||$filterStatus=='all'){
                $posts =  Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','pelanggan.nopel','pelanggan.nama_pel','user.nama as nama_petugas')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->where(DB::raw('upper(no_bonc)'), 'LIKE', "%{$search}%")
                    ->orWhere("realisasi.tgl_realisasi",$search2)
                    ->orWhere(DB::raw('text(tgl_realisasi)'), 'LIKE', "%{$search}%")
                    ->orWhere(DB::raw('upper(nama_pel)'), 'LIKE', "%{$search}%")
                    ->orWhere(DB::raw('upper(nopel)'), 'LIKE',  "%{$search}%")
                    ->orWhere(DB::raw('upper(nama)'), 'LIKE',  "%{$search}%")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

                $totalFiltered = Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','user.nama as nama_petugas','pelanggan.nopel','pelanggan.nama_pel')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->where(DB::raw('upper(no_bonc)'), 'LIKE', "%{$search}%")
                    ->orWhere("realisasi.tgl_realisasi",$search2)
                    ->orWhere(DB::raw('text(tgl_realisasi)'), 'LIKE', "%{$search}%")
                    ->orWhere(DB::raw('upper(nama_pel)'), 'LIKE', "%{$search}%")
                    ->orWhere(DB::raw('upper(nopel)'), 'LIKE',  "%{$search}%")
                    ->orWhere(DB::raw('upper(nama)'), 'LIKE',  "%{$search}%")
                    ->count();
            }
            elseif ($filterStatus=='new'){
                $filter =  Realisasi::where(['status_baru'=>0])->get();
                foreach ($filter as $item){
                    $status[]=$item->status_baru;
                }
                $posts =  Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','pelanggan.nopel','pelanggan.nama_pel','user.nama as nama_petugas')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
//                ->whereRaw("realisasi.tgl_realisasi:date LIKE '%$search%'")
                    ->whereIn('status_baru',$status)
                    ->where('realisasi.status_revisi',0)
                    ->where(function ($query) use ($search) {
                        $query->orWhere(DB::raw('upper(no_bonc)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nama_pel)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nopel)'), 'LIKE',  "%{$search}%")
                            ->orWhere(DB::raw('upper(nama)'), 'LIKE',  "%{$search}%");
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

                $totalFiltered = Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','user.nama as nama_petugas','pelanggan.nopel','pelanggan.nama_pel')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->whereIn('status_baru',$status)
                    ->where('realisasi.status_revisi',0)
                    ->where(function ($query) use ($search) {
                        $query->orWhere(DB::raw('upper(no_bonc)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nama_pel)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nopel)'), 'LIKE',  "%{$search}%")
                            ->orWhere(DB::raw('upper(nama)'), 'LIKE',  "%{$search}%");
                    })
                    ->count();
            }
            elseif ($filterStatus<=5){
                $filter =  Realisasi::where(['status_baru'=>$filterStatus])->get();
                foreach ($filter as $item){
                    $status[]=$item->status_baru;
                }
                $posts =  Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','pelanggan.nopel','pelanggan.nama_pel','user.nama as nama_petugas')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
//                ->whereRaw("realisasi.tgl_realisasi:date LIKE '%$search%'")
                    ->whereIn('status_baru',$status)
                    ->where(function ($query) use ($search) {
                        $query->orWhere(DB::raw('upper(no_bonc)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nama_pel)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nopel)'), 'LIKE',  "%{$search}%")
                            ->orWhere(DB::raw('upper(nama)'), 'LIKE',  "%{$search}%");
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

                $totalFiltered = Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','user.nama as nama_petugas','pelanggan.nopel','pelanggan.nama_pel')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->whereIn('status_baru',$status)
                    ->where(function ($query) use ($search) {
                        $query->orWhere(DB::raw('upper(no_bonc)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nama_pel)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nopel)'), 'LIKE',  "%{$search}%")
                            ->orWhere(DB::raw('upper(nama)'), 'LIKE',  "%{$search}%");
                    })
                    ->count();
            }
            elseif ($filterStatus==6){
                $filter =  Realisasi::where(['status_revisi'=>1])->get();
                foreach ($filter as $item){
                    $status[]=$item->status_revisi;
                }
                $posts =  Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','pelanggan.nopel','pelanggan.nama_pel','user.nama as nama_petugas')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
//                ->whereRaw("realisasi.tgl_realisasi:date LIKE '%$search%'")
                    ->whereIn('status_revisi',$status)
                    ->where(function ($query) use ($search) {
                        $query->orWhere(DB::raw('upper(no_bonc)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nama_pel)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nopel)'), 'LIKE',  "%{$search}%")
                            ->orWhere(DB::raw('upper(nama)'), 'LIKE',  "%{$search}%");
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

                $totalFiltered = Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','user.nama as nama_petugas','pelanggan.nopel','pelanggan.nama_pel')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->whereIn('status_revisi',$status)
                    ->where(function ($query) use ($search) {
                        $query->orWhere(DB::raw('upper(no_bonc)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nama_pel)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nopel)'), 'LIKE',  "%{$search}%")
                            ->orWhere(DB::raw('upper(nama)'), 'LIKE',  "%{$search}%");
                    })
                    ->count();
            }
            elseif ($filterStatus==7){
                $filter =  Realisasi::where(['status_revisi'=>2])->get();
                foreach ($filter as $item){
                    $status[]=$item->status_revisi;
                }
                $posts =  Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','pelanggan.nopel','pelanggan.nama_pel','user.nama as nama_petugas')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
//                ->whereRaw("realisasi.tgl_realisasi:date LIKE '%$search%'")
                    ->whereIn('status_revisi',$status)
                    ->where(function ($query) use ($search) {
                        $query->orWhere(DB::raw('upper(no_bonc)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nama_pel)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nopel)'), 'LIKE',  "%{$search}%")
                            ->orWhere(DB::raw('upper(nama)'), 'LIKE',  "%{$search}%");
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

                $totalFiltered = Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','user.nama as nama_petugas','pelanggan.nopel','pelanggan.nama_pel')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->whereIn('status_revisi',$status)
                    ->where(function ($query) use ($search) {
                        $query->orWhere(DB::raw('upper(no_bonc)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nama_pel)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nopel)'), 'LIKE',  "%{$search}%")
                            ->orWhere(DB::raw('upper(nama)'), 'LIKE',  "%{$search}%");
                    })
                    ->count();
            }
            elseif ($filterStatus==8){
                $filter =  Realisasi::where(['status_revisi'=>3])->get();
                foreach ($filter as $item){
                    $status[]=$item->status_revisi;
                }
                $posts =  Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','pelanggan.nopel','pelanggan.nama_pel','user.nama as nama_petugas')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
//                ->whereRaw("realisasi.tgl_realisasi:date LIKE '%$search%'")
                    ->whereIn('status_revisi',$status)
                    ->where(function ($query) use ($search) {
                        $query->orWhere(DB::raw('upper(no_bonc)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nama_pel)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nopel)'), 'LIKE',  "%{$search}%")
                            ->orWhere(DB::raw('upper(nama)'), 'LIKE',  "%{$search}%");
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

                $totalFiltered = Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tgl_realisasi',
                    'bonc.no_bonc','user.nama as nama_petugas','pelanggan.nopel','pelanggan.nama_pel')
                    ->join('bonc','bonc.id','=','bonc_id')
                    ->join('pelanggan','pelanggan.id','=','bonc.id_pel')
                    ->join('user','user.id','=','bonc.id_user')
                    ->whereIn('status_revisi',$status)
                    ->where(function ($query) use ($search) {
                        $query->orWhere(DB::raw('upper(no_bonc)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nama_pel)'), 'LIKE', "%{$search}%")
                            ->orWhere(DB::raw('upper(nopel)'), 'LIKE',  "%{$search}%")
                            ->orWhere(DB::raw('upper(nama)'), 'LIKE',  "%{$search}%");
                    })
                    ->count();
            }
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $num=>$post)
            {
                $nestedData['id'] = $num+1;
                $nestedData['status'] = ($post->status_revisi==0)?setStatusBaru($post->status_baru,$post->status_revisi):setStatusRevisi($post->status_revisi,$post->status_baru);
                $nestedData['tgl'] = date('d-m-Y',strtotime($post->tgl_realisasi));
                $nestedData['bonc_id'] = $post->no_bonc;
                $nestedData['nama_petugas'] = $post->nama_petugas;
                $nestedData['nopel'] = $post->nopel;
                $nestedData['pelanggan'] = $post->nama_pel;
                $nestedData['aksi'] = "
                    <a href='".(url('backend/data/realisasi/detail?id=').''.$post->id.'')."' class='btn btn-info btn-s'>Detail</a>
                    ";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }
}
