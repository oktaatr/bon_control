<?php

namespace App\Http\Controllers;

use App\Classes\Captcha;
use App\Models\User;
use Illuminate\Http\Request;
use Session;

class LoginController extends Controller
{
    private $captcha;
    public function __construct()
    {
        $this->captcha=new Captcha();
    }

    public function index(Request $request){
        if ($request->session()->has('activeUser')) {
            return redirect('/backend');
        }
//        $params=[
//            'captcha'=>$this->captcha->createCaptcha(),
//        ];
        return view('login.index');
    }

    public  function login(Request $request){
        $nip=$request->input('login');
        $username=$request->input('login');
        $captcha=$request->input('captcha');
        $password=$request->input('password');

        $activeUser=User::where(['username'=>$username])
            ->orWhere(['nip'=>$nip])
            ->first();

        if(is_null($activeUser)){
            return "<div class='alert alert-danger'>Pengguna Tidak Ditemukan!</div>
                    <script> scrollToTop(); reload(1000); </script>";

        }else{
            if($activeUser->password !=sha1($password)){
                return "<div class='alert alert-danger'>Password Salah!</div>
                        <script> scrollToTop(); reload(1000); </script>";
            }
            elseif($captcha!=$request->session()->get('LoginCaptcha')){
                return "
            <div class='alert alert-danger'>Captcha tidak valid!</div>
            <script> scrollToTop(); reload(1000); </script>";
            }
            else{
                $request->session()->put('activeUser', $activeUser);

                return "
            <div class='alert alert-success'>Login berhasil!</div>
            <script> scrollToTop(); reload(1000); </script>";
            }
        }


    }

    public function captcha(Request $request)
    {
//        if(session()->has('captcha')){
//            session()->forget('captcha');
//        }
        $num_chars=6;//number of characters for captcha image
        $characters=array_merge(range(0,9),range('A','Z'),range('a','z'));//creating combination of numbers & alphabets
        shuffle($characters);//shuffling the characters
//        $captcha_text="";
//        for($i=0;$i<$num_chars;$i++){
//            $captcha_text.=$characters[rand(0,count($characters)-1)];
//        }
//        session()->put('captcha',$captcha_text);
        header("Content-type: image/png");// setting the content type as png
        $captcha_image=imagecreatetruecolor(190,70);
        $captcha_background=imagecolorallocate($captcha_image,225,238,221);//setting captcha background colour
        $captcha_text_colour=imagecolorallocate($captcha_image,10, 106, 161);//setting cpatcha text colour
        imagefilledrectangle($captcha_image,0,0,190,70,$captcha_background);//creating the rectangle
        $font=public_path('captcha.ttf');//setting the font path
        imagettftext($captcha_image,30,10,24,59,$captcha_text_colour,$font,$this->captcha->createCaptcha());
        imagepng($captcha_image);
        imagedestroy($captcha_image);
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/');
    }


}