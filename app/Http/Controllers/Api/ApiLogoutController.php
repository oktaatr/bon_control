<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/05/18
 * Time: 00.14
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;

class ApiLogoutController extends Controller
{
    public function logout(Request $request){
        $nip=$request->nip;
        $password=sha1($request->password);
        $token=$request->token;

//        $sendingParams = [
//            'nip' => $login,
//            'username'=>$login,
//            'password' => $password
//        ];

//        if(is_null($login)){
//            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter nip!',json_encode($sendingParams));
//        }
//
        $cekRole=User::where('role_id',2);
        $activeUser=User::where(['nip'=>$nip])
//            ->orWhere(['username'=>$login])
            ->first();
//        if(is_null($activeUser)){
//            return $this->messageSystem->returnApiMessage($apiName, 404, "Username not found!", json_encode($sendingParams) );
//        }
//
//        if($activeUser->password != $password){
//            return $this->messageSystem->returnApiMessage($apiName, 401, "Password not match!", json_encode($sendingParams) );
//        }

        if (is_null($nip)){
            $data = [
                'message' => 'Data kosong',
            ];
            return response()->json($data);
        }
        if($cekRole){
            if ($activeUser){
                $activeUser->token=null;
                $activeUser->save();
                $data = [
                    'id' => $activeUser->id,
                    'username' => $activeUser->username,
                    'nip' => $activeUser->nip,
                    'nama'=>$activeUser->nama_pel,
                    'token'=>$activeUser->token,
                ];

                $params = [
                    'code' => 302,
                    'description' => 'Found',
                    'message' => 'Logout Success!',
                    'data' => $data
                ];
                return response()->json($params);
            }
            else{
                $data = [
                    'message' => 'Username salah',
                ];
                return response()->json($data);

            }

        }

        if($activeUser->password != $password){
            $data = [
                'message' => 'Password salah',
            ];
            return response()->json($data);
        }
    }

}