<?php

namespace App\Http\Controllers\Api;

use App\Classes\MessageSystemClass;
use App\Models\Bonc;
use App\Models\PemakaianAir;
use App\Models\Realisasi;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ApiBoncController extends Controller
{
    private $messageSystem;

    public function __construct()
    {
        $this->messageSystem=new MessageSystemClass();
    }

    public function getBoncBaru($idPetugas){
        $apiName='BONC_Baru';
        $sendingParams=[
            'id_user'=>$idPetugas
        ];
        if (is_null($idPetugas)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter id_user!',json_encode($sendingParams));
        }
        $cekbBaru=Realisasi::where(['status_baru'=>0,'status_revisi'=>0])->first();
        if($cekbBaru) {
            $cekBaru=Realisasi::where(['status_baru'=>0,'status_revisi'=>0])->get();
            foreach ($cekBaru as $item){
                $id_bonc[]=$item->bonc_id;
            }
            $databaru=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->first();
            if($databaru) {
                $data = Bonc::whereIn('id', $id_bonc)->where(['id_user' => $idPetugas])->get();
                foreach ($data as $item) {
                    $databon[] = [
                        'id' => $item->id,
                        'nomerbon' => $item->no_bonc,
                        'tglbon' => $item->tglbonc,
                        'namapel' => $item->getPelanggan->nama_pel,
                        'nopel' => $item->getPelanggan->nopel,
                        'alamatpel' => $item->getPelanggan->alamat,
                        'masalah' => $item->masalah,
                        'ket_realisasi'=>$item->getBonc->ket_realisasi
                    ];
                }

                $params = [
                    'code' => 302,
                    'description' => 'found',
                    'messaage' => 'Bonc berhasil di dapatkan',
                    'bonc' => $databon
                ];
                return response()->json($params);
            }
            else{
                $params = [
                    'code' => 500,
                    'description' => 'found',
                    'messaage' => 'Tidak ada Bonc tersimpan',

                ];
                return response()->json($params);
            }
        }
        else {
            $params = [
                'code' => 500,
                'description' => 'found',
                'messaage' => 'Tidak ada Bonc tersimpan',

            ];
            return response()->json($params);
        }
    }

    public function getBoncProses($idPetugas){
        $apiName='BONC_Proses';
        $sendingParams=[
            'id_user'=>$idPetugas
        ];
        if (is_null($idPetugas)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter id_user!',json_encode($sendingParams));
        }
        $cekProses=Realisasi::where(['status_baru'=>5,'status_revisi'=>0])->first();
        if($cekProses) {
            $cekBaru=Realisasi::where(['status_baru'=>5,'status_revisi'=>0])->get();
            foreach ($cekBaru as $item){
                $id_bonc[]=$item->bonc_id;
            }
            $dataproses=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->first();
            if($dataproses) {
                $data = Bonc::whereIn('id', $id_bonc)->where(['id_user' => $idPetugas])->get();
                foreach ($data as $item) {
                    $databon[] = [
                        'id' => $item->id,
                        'nomerbon' => $item->no_bonc,
                        'tglbon' => $item->tglbonc,
                        'namapel' => $item->getPelanggan->nama_pel,
                        'nopel' => $item->getPelanggan->nopel,
                        'alamatpel' => $item->getPelanggan->alamat,
                        'masalah' => $item->masalah,
                        'ket_realisasi'=>$item->getBonc->ket_realisasi

                    ];
                }

                $params = [
                    'code' => 302,
                    'description' => 'found',
                    'messaage' => 'Bonc berhasil di dapatkan',
                    'bonc' => $databon
                ];
                return response()->json($params);
            }
            else{
                $params = [
                    'code' => 500,
                    'description' => 'found',
                    'messaage' => 'Tidak ada Bonc tersimpan',

                ];
                return response()->json($params);
            }
        }
        else {
            $params = [
                'code' => 500,
                'description' => 'found',
                'messaage' => 'Tidak ada Bonc tersimpan',

            ];
            return response()->json($params);
        }
    }

    public function getBoncKirim($idPetugas){
        $apiName='BONC_Kirim';
        $sendingParams=[
            'id_user'=>$idPetugas
        ];
        if (is_null($idPetugas)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter id_user!',json_encode($sendingParams));
        }
        $cekKirim=Realisasi::where(['status_baru'=>1,'status_revisi'=>0])->first();
        if($cekKirim) {
            $cekBaru=Realisasi::where(['status_baru'=>1,'status_revisi'=>0])->get();
            foreach ($cekBaru as $item){
                $id_bonc[]=$item->bonc_id;
            }
            $datakirim=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->first();
            if($datakirim) {
                $data = Bonc::whereIn('id', $id_bonc)->where(['id_user' => $idPetugas])->get();
                foreach ($data as $item) {
                    $databon[] = [
                        'id' => $item->id,
                        'nomerbon' => $item->no_bonc,
                        'tglbon' => $item->tglbonc,
                        'namapel' => $item->getPelanggan->nama_pel,
                        'nopel' => $item->getPelanggan->nopel,
                        'alamatpel' => $item->getPelanggan->alamat,
                        'masalah' => $item->masalah,
                        'ket_realisasi'=>$item->getBonc->ket_realisasi
                    ];
                }
                $params = [
                    'code' => 302,
                    'description' => 'found',
                    'messaage' => 'Bonc berhasil di dapatkan',
                    'bonc' => $databon
                ];
                return response()->json($params);
            }
            else{
                $params = [
                    'code' => 500,
                    'description' => 'found',
                    'messaage' => 'Tidak ada Bonc terkirim',

                ];
                return response()->json($params);
            }
        }
        else {
            $params = [
                'code' => 500,
                'description' => 'found',
                'messaage' => 'Tidak ada Bonc terkirim',

            ];
            return response()->json($params);
        }
    }

    public function getDetailBonc($idBonc){
        $apiName='Detail_BONC';
        $sendingParams=[
            'id'=>$idBonc
        ];
        if (is_null($idBonc)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter id BONC!',json_encode($sendingParams));
        }
        $dataBonc=Bonc::where(['id'=>$idBonc])->first();
        $TglBonc=new Carbon($dataBonc->tglbonc);
        $dataAir=PemakaianAir::where('idpel',$dataBonc->id_pel)->get();
        foreach ($dataAir as $item){
            $idpel[]=$item->id;
        }
        $datapemair=PemakaianAir::whereIn('idpel',[$dataBonc->id_pel])->where("tglcatat",">=", $TglBonc->subMonths(7))->get();
        $detail=[
            'id' => $dataBonc->id,
            'nobonc' => $dataBonc->no_bonc,
            'tglbonc' => $dataBonc->tglbonc,
            'nopel' => $dataBonc->getPelanggan->nopel,
            'nama' => $dataBonc->getPelanggan->nama_pel,
            'alamat' => $dataBonc->getPelanggan->alamat,
            'sumberdt' => $dataBonc->sbrdt,
            'pengaduan' => $dataBonc->asal_adu,
            'retribusi' => $dataBonc->getPelanggan->retrib,
            'kodetarip' => $dataBonc->getPelanggan->kdtrp,
            'nopa' => $dataBonc->getPelanggan->nopa,
            'datamtr' => $dataBonc->getPelanggan->dtmtr,
            'masalah' => $dataBonc->masalah,

        ];
        $params=[
            'code'=>302,
            'description'=>'found',
            'messaage'=>'Detail berhasil di dapatkan',
            'detail'=>$detail,
            'pemakaianair'=>$datapemair
        ];
        return response()->json($params);
    }

    public function countBaru($idPetugas){
        $Baru=Realisasi::where(['status_baru'=>0,'status_revisi'=>0])->first();
        $Proses=Realisasi::where(['status_baru'=>5])->first();
        $Kirim=Realisasi::where(['status_baru'=>1])->first();

        $cekBaru=Realisasi::where(['status_baru'=>0,'status_revisi'=>0])->get();
        $cekProses=Realisasi::where(['status_baru'=>5])->get();
        $cekKirim=Realisasi::where(['status_baru'=>1])->get();

        if($Baru && $Proses && $Kirim){
            foreach ($cekBaru as $item){
                $id_boncB[]=$item->bonc_id;
            }
            foreach ($cekProses as $item){
                $id_bonc[]=$item->bonc_id;
            }
            foreach ($cekKirim as $item){
                $id_boncK[]=$item->bonc_id;
            }
            $dataBaru=Bonc::whereIn('id',$id_boncB)->where(['id_user'=>$idPetugas])->count();
            $dataProses=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->count();
            $dataKirim=Bonc::whereIn('id',$id_boncK)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => $dataBaru,
                'proses' => $dataProses,
                'kirim' => $dataKirim
            ];
            return response()->json($params);
        }

        elseif($Baru&&$Proses || $Proses&&$Baru){
            foreach ($cekBaru as $item){
                $id_boncB[]=$item->bonc_id;
            }
            foreach ($cekProses as $item){
                $id_bonc[]=$item->bonc_id;
            }
            $dataBaru=Bonc::whereIn('id',$id_boncB)->where(['id_user'=>$idPetugas])->count();
            $dataProses=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => $dataBaru,
                'proses' => $dataProses,
                'kirim' => 0
            ];
            return response()->json($params);
        }

        elseif($Baru&&$Kirim || $Kirim&&$Baru){
            foreach ($cekBaru as $item){
                $id_boncB[]=$item->bonc_id;
            }
            foreach ($cekKirim as $item){
                $id_boncK[]=$item->bonc_id;
            }
            $dataBaru=Bonc::whereIn('id',$id_boncB)->where(['id_user'=>$idPetugas])->count();
            $dataKirim=Bonc::whereIn('id',$id_boncK)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => $dataBaru,
                'proses' => 0,
                'kirim' => $dataKirim
            ];
            return response()->json($params);
        }

        elseif($Proses&&$Kirim || $Kirim&&$Proses){
            foreach ($cekProses as $item){
                $id_bonc[]=$item->bonc_id;
            }
            foreach ($cekKirim as $item){
                $id_boncK[]=$item->bonc_id;
            }
            $dataProses=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->count();
            $dataKirim=Bonc::whereIn('id',$id_boncK)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => 0,
                'proses' => $dataProses,
                'kirim' => $dataKirim
            ];
            return response()->json($params);
        }

        elseif($Baru){
            foreach ($cekBaru as $item){
                $id_boncB[]=$item->bonc_id;
            }
            $dataBaru=Bonc::whereIn('id',$id_boncB)->where(['id_user'=>$idPetugas])->count();
            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => $dataBaru,
                'proses' => 0,
                'kirim' => 0
            ];
            return response()->json($params);
        }

        elseif($Proses){
            foreach ($cekProses as $item){
                $id_bonc[]=$item->bonc_id;
            }
            $dataProses=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => 0,
                'proses' => $dataProses,
                'kirim' => 0
            ];
            return response()->json($params);
        }

        elseif($Kirim){
            foreach ($cekKirim as $item){
                $id_boncK[]=$item->bonc_id;
            }
            $dataKirim=Bonc::whereIn('id',$id_boncK)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => 0,
                'proses' => 0,
                'kirim' => $dataKirim
            ];
            return response()->json($params);
        }

        $params = [
            'code' => 302,
            'description' => 'found',
            'messaage' => 'Bonc berhasil di dapatkan',
            'baru' => 0,
            'proses' => 0,
            'kirim' => 0
        ];
        return response()->json($params);
    }
    //notifikasi
    public function notifBaru($idPetugas){
        $Baru=Realisasi::where(['status_baru'=>0,'status_revisi'=>0])->first();
        $Proses=Realisasi::where(['status_baru'=>5])->first();

        $revBaru=Realisasi::where(['status_revisi'=>1])->first();
        $revProses=Realisasi::where(['status_revisi'=>2])->first();

        $cekrevBaru=Realisasi::where(['status_revisi'=>1])->get();
        $cekrevProses=Realisasi::where(['status_revisi'=>2])->get();

        $cekBaru=Realisasi::where(['status_baru'=>0,'status_revisi'=>0])->get();
        $cekProses=Realisasi::where(['status_baru'=>5])->get();
        if(!is_null($Baru)) {
            foreach ($cekBaru as $item) {
                $id_boncB[] = $item->bonc_id;
                $dataBaru = Bonc::whereIn('id', $id_boncB)->where(['id_user' => $idPetugas])->count();
            }
        }
        else{
            $dataBaru=0;
        }
        if(!is_null($Proses)) {
            foreach ($cekProses as $item) {
                $id_bonc[] = $item->bonc_id;
                $dataProses = Bonc::whereIn('id', $id_bonc)->where(['id_user' => $idPetugas])->count();
            }
        }
        else{
            $dataProses=0;
        }
        if(!is_null($revBaru)) {
            foreach ($cekrevBaru as $item) {
                $id_boncrevB[] = $item->bonc_id;
                $datarevBaru = Bonc::whereIn('id', $id_boncrevB)->where(['id_user' => $idPetugas])->count();
            }
        }
        else{
            $datarevBaru=0;
        }
        if(!is_null($revProses)) {
            foreach ($cekrevProses as $item) {
                $id_revbonc[] = $item->bonc_id;
                $datarevProses = Bonc::whereIn('id', $id_revbonc)->where(['id_user' => $idPetugas])->count();
            }
        }
        else{
            $datarevProses=0;
        }

        $params = [
            'code' => 302,
            'description' => 'found',
            'messaage' => 'Bonc berhasil di dapatkan',
            'notifbaru' => $dataBaru + $dataProses,
            'notifrevisi' => $datarevBaru + $datarevProses,
        ];
        return response()->json($params);
    }

    //Revisi
    public function getBoncRevisiBaru($idPetugas){
        $apiName='BONC_Revisi_Baru';
        $sendingParams=[
            'id_user'=>$idPetugas
        ];
        if (is_null($idPetugas)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter id_user!',json_encode($sendingParams));
        }
        $cekbBaru=Realisasi::where(['status_revisi'=>1,'status_baru'=>0])->first();
        if($cekbBaru) {
            $cekBaru=Realisasi::where(['status_revisi'=>1,'status_baru'=>0])->get();
            foreach ($cekBaru as $item){
                $id_bonc[]=$item->bonc_id;
            }
            $datarevisi=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->first();
            if($datarevisi) {
                $data = Bonc::whereIn('id', $id_bonc)->where(['id_user' => $idPetugas])->get();
                foreach ($data as $item) {
                    $databon[] = [
                        'id' => $item->id,
                        'nomerbon' => $item->no_bonc,
                        'tglbon' => $item->tglbonc,
                        'namapel' => $item->getPelanggan->nama_pel,
                        'nopel' => $item->getPelanggan->nopel,
                        'alamatpel' => $item->getPelanggan->alamat,
                        'masalah' => $item->masalah,
                        'ket_realisasi'=>$item->getBonc->ket_realisasi
                    ];
                }

                $params = [
                    'code' => 302,
                    'description' => 'found',
                    'messaage' => 'Bonc berhasil di dapatkan',
                    'bonc' => $databon
                ];
                return response()->json($params);
            }
            else{
                $params = [
                    'code' => 500,
                    'description' => 'found',
                    'messaage' => 'Tidak ada Bonc tersimpan',

                ];
                return response()->json($params);
            }
        }
        else {
            $params = [
                'code' => 500,
                'description' => 'found',
                'messaage' => 'Tidak ada Bonc tersimpan',

            ];
            return response()->json($params);
        }
    }

    public function getBoncRevisiProses($idPetugas){
        $apiName='BONC_Revisi_Proses';
        $sendingParams=[
            'id_user'=>$idPetugas
        ];
        if (is_null($idPetugas)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter id_user!',json_encode($sendingParams));
        }
        $cekProses=Realisasi::where(['status_revisi'=>2,'status_baru'=>0])->first();
        if($cekProses) {
            $cekBaru=Realisasi::where(['status_revisi'=>2,'status_baru'=>0])->get();
            foreach ($cekBaru as $item){
                $id_bonc[]=$item->bonc_id;
            }
            $dataproses=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->first();
            if($dataproses) {
                $data = Bonc::whereIn('id', $id_bonc)->where(['id_user' => $idPetugas])->get();
                foreach ($data as $item) {
                    $databon[] = [
                        'id' => $item->id,
                        'nomerbon' => $item->no_bonc,
                        'tglbon' => $item->tglbonc,
                        'namapel' => $item->getPelanggan->nama_pel,
                        'nopel' => $item->getPelanggan->nopel,
                        'alamatpel' => $item->getPelanggan->alamat,
                        'masalah' => $item->masalah,
                        'ket_realisasi'=>$item->getBonc->ket_realisasi
                    ];
                }

                $params = [
                    'code' => 302,
                    'description' => 'found',
                    'messaage' => 'Bonc berhasil di dapatkan',
                    'bonc' => $databon
                ];
                return response()->json($params);
            }
            else{
                $params = [
                    'code' => 500,
                    'description' => 'found',
                    'messaage' => 'Tidak ada Bonc tersimpan',

                ];
                return response()->json($params);
            }
        }
        else {
            $params = [
                'code' => 500,
                'description' => 'found',
                'messaage' => 'Tidak ada Bonc tersimpan',

            ];
            return response()->json($params);
        }
    }

    public function getBoncRevisiKirim($idPetugas){
        $apiName='BONC_Kirim';
        $sendingParams=[
            'id_user'=>$idPetugas
        ];
        if (is_null($idPetugas)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter id_user!',json_encode($sendingParams));
        }
        $cekKirim=Realisasi::where(['status_revisi'=>3,'status_baru'=>0])->first();
        if($cekKirim) {
            $cekBaru=Realisasi::where(['status_revisi'=>3,'status_baru'=>0])->get();
            foreach ($cekBaru as $item){
                $id_bonc[]=$item->bonc_id;
            }
            $datakirim=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->first();
            if($datakirim) {
                $data = Bonc::whereIn('id', $id_bonc)->where(['id_user' => $idPetugas])->get();
                foreach ($data as $item) {
                    $databon[] = [
                        'id' => $item->id,
                        'nomerbon' => $item->no_bonc,
                        'tglbon' => $item->tglbonc,
                        'namapel' => $item->getPelanggan->nama_pel,
                        'nopel' => $item->getPelanggan->nopel,
                        'alamatpel' => $item->getPelanggan->alamat,
                        'masalah' => $item->masalah,
                        'ket_realisasi'=>$item->getBonc->ket_realisasi
                    ];
                }
                $params = [
                    'code' => 302,
                    'description' => 'found',
                    'messaage' => 'Bonc berhasil di dapatkan',
                    'bonc' => $databon
                ];
                return response()->json($params);
            }
            else{
                $params = [
                    'code' => 500,
                    'description' => 'found',
                    'messaage' => 'Tidak ada Bonc terkirim',

                ];
                return response()->json($params);
            }
        }
        else {
            $params = [
                'code' => 500,
                'description' => 'found',
                'messaage' => 'Tidak ada Bonc terkirim',

            ];
            return response()->json($params);
        }
    }
    //Count Revisi
    public function countRevisi($idPetugas){
        $Baru=Realisasi::where(['status_revisi'=>1])->first();
        $Proses=Realisasi::where(['status_revisi'=>2])->first();
        $Kirim=Realisasi::where(['status_revisi'=>3])->first();

        $cekBaru=Realisasi::where(['status_revisi'=>1])->get();
        $cekProses=Realisasi::where(['status_revisi'=>2])->get();
        $cekKirim=Realisasi::where(['status_revisi'=>3])->get();

        if($Baru && $Proses && $Kirim){
            foreach ($cekBaru as $item){
                $id_boncB[]=$item->bonc_id;
            }
            foreach ($cekProses as $item){
                $id_bonc[]=$item->bonc_id;
            }
            foreach ($cekKirim as $item){
                $id_boncK[]=$item->bonc_id;
            }
            $dataBaru=Bonc::whereIn('id',$id_boncB)->where(['id_user'=>$idPetugas])->count();
            $dataProses=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->count();
            $dataKirim=Bonc::whereIn('id',$id_boncK)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => $dataBaru,
                'proses' => $dataProses,
                'kirim' => $dataKirim
            ];
            return response()->json($params);
        }

        elseif($Baru&&$Proses || $Proses&&$Baru){
            foreach ($cekBaru as $item){
                $id_boncB[]=$item->bonc_id;
            }
            foreach ($cekProses as $item){
                $id_bonc[]=$item->bonc_id;
            }
            $dataBaru=Bonc::whereIn('id',$id_boncB)->where(['id_user'=>$idPetugas])->count();
            $dataProses=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => $dataBaru,
                'proses' => $dataProses,
                'kirim' => 0
            ];
            return response()->json($params);
        }

        elseif($Baru&&$Kirim || $Kirim&&$Baru){
            foreach ($cekBaru as $item){
                $id_boncB[]=$item->bonc_id;
            }
            foreach ($cekKirim as $item){
                $id_boncK[]=$item->bonc_id;
            }
            $dataBaru=Bonc::whereIn('id',$id_boncB)->where(['id_user'=>$idPetugas])->count();
            $dataKirim=Bonc::whereIn('id',$id_boncK)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => $dataBaru,
                'proses' => 0,
                'kirim' => $dataKirim
            ];
            return response()->json($params);
        }

        elseif($Proses&&$Kirim || $Kirim&&$Proses){
            foreach ($cekProses as $item){
                $id_bonc[]=$item->bonc_id;
            }
            foreach ($cekKirim as $item){
                $id_boncK[]=$item->bonc_id;
            }
            $dataProses=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->count();
            $dataKirim=Bonc::whereIn('id',$id_boncK)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => 0,
                'proses' => $dataProses,
                'kirim' => $dataKirim
            ];
            return response()->json($params);
        }

        elseif($Baru){
            foreach ($cekBaru as $item){
                $id_boncB[]=$item->bonc_id;
            }
            $dataBaru=Bonc::whereIn('id',$id_boncB)->where(['id_user'=>$idPetugas])->count();
            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => $dataBaru,
                'proses' => 0,
                'kirim' => 0
            ];
            return response()->json($params);
        }

        elseif($Proses){
            foreach ($cekProses as $item){
                $id_bonc[]=$item->bonc_id;
            }
            $dataProses=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => 0,
                'proses' => $dataProses,
                'kirim' => 0
            ];
            return response()->json($params);
        }

        elseif($Kirim){
            foreach ($cekKirim as $item){
                $id_boncK[]=$item->bonc_id;
            }
            $dataKirim=Bonc::whereIn('id',$id_boncK)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'baru' => 0,
                'proses' => 0,
                'kirim' => $dataKirim
            ];
            return response()->json($params);
        }

        $params = [
            'code' => 302,
            'description' => 'found',
            'messaage' => 'Bonc berhasil di dapatkan',
            'baru' => 0,
            'proses' => 0,
            'kirim' => 0
        ];
        return response()->json($params);


    }
    //notifRevisi
    public function notifRevisi($idPetugas){
        $Baru=Realisasi::where(['status_revisi'=>1])->first();
        $Proses=Realisasi::where(['status_revisi'=>2])->first();

        $cekBaru=Realisasi::where(['status_revisi'=>1])->get();
        $cekProses=Realisasi::where(['status_revisi'=>2])->get();

        if($Baru && $Proses){
            foreach ($cekBaru as $item){
                $id_boncB[]=$item->bonc_id;
            }
            foreach ($cekProses as $item){
                $id_bonc[]=$item->bonc_id;
            }

            $dataBaru=Bonc::whereIn('id',$id_boncB)->where(['id_user'=>$idPetugas])->count();
            $dataProses=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'notifbaru' => $dataBaru+$dataProses,
            ];
            return response()->json($params);
        }

        elseif($Baru&&$Proses || $Proses&&$Baru){
            foreach ($cekBaru as $item){
                $id_boncB[]=$item->bonc_id;
            }
            foreach ($cekProses as $item){
                $id_bonc[]=$item->bonc_id;
            }
            $dataBaru=Bonc::whereIn('id',$id_boncB)->where(['id_user'=>$idPetugas])->count();
            $dataProses=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'notifbaru' => $dataBaru+$dataProses,

            ];
            return response()->json($params);
        }

        elseif($Baru){
            foreach ($cekBaru as $item){
                $id_boncB[]=$item->bonc_id;
            }
            $dataBaru=Bonc::whereIn('id',$id_boncB)->where(['id_user'=>$idPetugas])->count();
            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'notifbaru' => $dataBaru,

            ];
            return response()->json($params);
        }

        elseif($Proses){
            foreach ($cekProses as $item){
                $id_bonc[]=$item->bonc_id;
            }
            $dataProses=Bonc::whereIn('id',$id_bonc)->where(['id_user'=>$idPetugas])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'messaage' => 'Bonc berhasil di dapatkan',
                'notifbaru' => $dataProses,

            ];
            return response()->json($params);
        }

        $params = [
            'code' => 302,
            'description' => 'found',
            'messaage' => 'Bonc berhasil di dapatkan',
            'notifbaru' => 0,

        ];
        return response()->json($params);


    }
}