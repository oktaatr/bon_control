<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/05/18
 * Time: 00.49
 */

namespace App\Http\Controllers\Api;

use App\Classes\MessageSystemClass;
use App\Http\Controllers\Controller;
use App\Models\User;

class ApiProfileController extends Controller
{
    private $messageSystem;

    public function __construct()
    {
        $this->messageSystem=new MessageSystemClass();
    }
    public function getProfile($iduser){
        $apiName='GET_PROFILE';
        $sendingParams=[
            'id_user'=>$iduser
        ];
        if (is_null($iduser)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter id_user!',json_encode($sendingParams));
        }
        $profile=User::where(['id'=>$iduser])->first();
        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Found Success!',
            'id' => $profile->id,
            'nama'=>$profile->nama,
            'nip'=>$profile->nip
        ];
        return response()->json($params);
    }
}