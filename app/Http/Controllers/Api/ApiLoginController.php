<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/05/18
 * Time: 00.14
 */

namespace App\Http\Controllers\Api;


use App\Classes\MessageSystemClass;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class ApiLoginController extends Controller
{
    private  $messageSystem;

    public  function __construct()
    {
        $this->messageSystem= new MessageSystemClass();
    }

    public function login(Request $request){
        $apiName='VALIDATE_LOGIN';
        $nip=$request->nip;
        $imei=$request->imei;
        $password=sha1($request->password);

        $sendingParams = [
            'nip' => $nip,
            'password' => $password,
        ];

        if(is_null($nip)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter nip!',json_encode($sendingParams));
        }

        $activeUser=User::where(['nip'=>$nip,'role_id'=>3])->first();

        if(is_null($activeUser)){
            return $this->messageSystem->returnApiMessage($apiName, 404, "Username not found!", json_encode($sendingParams) );
        }

        if($activeUser->password != $password){
            return $this->messageSystem->returnApiMessage($apiName, 401, "Password not match!", json_encode($sendingParams) );
        }

        if($activeUser->imei != $imei){
            return $this->messageSystem->returnApiMessage($apiName, 401, "Device tidak diizinkan masuk, hubungi admin!", json_encode($sendingParams) );
        }

        $data = [
            'id' => $activeUser->id,
            'nip' => $activeUser->nip,
            'nama'=>$activeUser->nama,
        ];
        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Login Success!',
            'data' => $data
        ];
        return response()->json($params);

    }
}