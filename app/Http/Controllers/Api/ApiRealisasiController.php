<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/05/18
 * Time: 00.50
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Classes\MessageSystemClass;
use Illuminate\Http\Request;
use App\Models\Bonc;
use App\Models\Realisasi;
use App\Models\RealisasiFoto;
use Illuminate\Support\Facades\File;


class ApiRealisasiController extends Controller
{
    private $messageSystem;
    public function __construct()
    {
        $this->messageSystem=new MessageSystemClass();
    }
    public function GetRealisasiBaru($idRealisasi){
        $apiName='REVISI_REALISASI';
        $getRealisasiB=Realisasi::where(['id'=>$idRealisasi])->first();
        $sendingParams=[
            'id'=>$idRealisasi,
        ];
        if(is_null($getRealisasiB)){
            return $this->messageSystem->returnApiMessage($apiName,404,'id not found',json_encode($sendingParams));
        }
        $dataFotoRealisasi=RealisasiFoto::where(['id_realisasi'=>$getRealisasiB->id])->get();
        $datafoto=[];
        foreach ($dataFotoRealisasi as $fotobonc){
            $datafoto=[
                'foto'=>'public/foto_bonc/'.$fotobonc->foto,
            ];
        }

        $params=[
            'code'=>302,
            'description'=>'found',
            'message'=>'Data berhasil di dapatkan',
            'data'=>[
                'hasil'=>$getRealisasiB,
                'RealisasiFoto'=>$datafoto,
            ]
        ];
        return response()->json($params);
    }

    public function inputHasilKontrol($idBonc,Request $request){
        $apiName='INPUT_REALISASI';
        $stndmtr=$request->stndmtr;
        $tgl_entry=$request->tgl_entry;
        $nomtr=$request->nomtr;
        $ukuranmtr=$request->ukuranmtr;
        $merkmtr=$request->merkmtr;
        $kondmtr=$request->kondmtr;
        $kondair=$request->kondair;
        $jammulai_air=$request->jammulai_air;
        $jamakhir_air=$request->jamakhir_air;
        $sglmtr=$request->sglmtr;
        $sglkopling=$request->sglkopling;
        $pipahubung=$request->pipahubung;
        $ltkmtr=$request->ltkmtr;
        $mincharge=$request->mincharge;
        $pelpgl=$request->pelpgl;
        $gnpersil=$request->gnpersil;
        $jumjiwa=$request->jumjiwa;
        $sumur=$request->sumur;
        $telpel=$request->telpel;
        $sitpersil=$request->sitpersil;
        $keterangan=$request->keterangan;
        $tind_lanjut=$request->tind_lanjut;
        $tgl_realisasi=date('Y-m-d');
        $bonc_id=$request->bonc_id;
        $picture = $request->file('fotometer');
        $picture2 = $request->file('fotopersil');
        $picture3 = $request->file('fotopelanggan');
        $picture4 = $request->file('fotottd');
        $sendingParams=[
            'stand_meter'=>$stndmtr,
            'tanggal_entry'=>$tgl_entry,
            'no_meter'=>$nomtr,
            'ukuran_meter'=>$ukuranmtr,
            'merk_meter'=>$merkmtr,
            'kondmtr'=>$kondmtr,
            'kondisi_air'=>$kondair,
            'jammulai_air'=>$jammulai_air,
            'jamakhir_air'=>$jamakhir_air,
            'segel_meter'=>$sglmtr,
            'segel_kopling'=>$sglkopling,
            'pipa_hubung'=>$pipahubung,
            'letak_meter'=>$ltkmtr,
            'minimum_charge'=>$mincharge,
            'pelanggan_panggil'=>$pelpgl,
            'guna_persil'=>$gnpersil,
            'jumlah_jiwa'=>$jumjiwa,
            'sumur'=>$sumur,
            'tlp_pelanggan'=>$telpel,
            'situasi_persil'=>$sitpersil,
            'keterangan'=>$keterangan,
            'tindak_lanjut'=>$tind_lanjut,
            'tgl_realisasi'=>$tgl_realisasi,
            'bonc_id'=>$bonc_id
        ];

        if (is_null($stndmtr)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'standmeter not found!',json_encode($sendingParams));
        }
        if (is_null($tgl_entry)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'tglentry not found!',json_encode($sendingParams));
        }
        if (is_null($nomtr)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'nometer not found!',json_encode($sendingParams));
        }
        if (is_null($ukuranmtr)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'ukuranmeter not found!',json_encode($sendingParams));
        }
        if (is_null($merkmtr)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'merkmeter not found!',json_encode($sendingParams));
        }
        if (is_null($kondmtr)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'kondmeter not found!',json_encode($sendingParams));
        }
        if (is_null($kondair)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'kondair not found!',json_encode($sendingParams));
        }
        if (is_null($jammulai_air)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'jammulai not found!',json_encode($sendingParams));
        }
        if (is_null($jamakhir_air)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'jamakhir not found!',json_encode($sendingParams));
        }
        if (is_null($sglkopling)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'segelkopling not found!',json_encode($sendingParams));
        }
        if (is_null($sglmtr)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'segelmotor not found!',json_encode($sendingParams));
        }
        if (is_null($pipahubung)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'pipahubung not found!',json_encode($sendingParams));
        }
        if (is_null($ltkmtr)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'letakmeter not found!',json_encode($sendingParams));
        }
        if (is_null($mincharge)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'min charge not found!',json_encode($sendingParams));
        }
        if (is_null($pelpgl)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'pelpgl not found!',json_encode($sendingParams));
        }if (is_null($telpel)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'tlp pel not found!',json_encode($sendingParams));
        }
        if (is_null($gnpersil)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'gunapersil not found!',json_encode($sendingParams));
        }
        if (is_null($jumjiwa)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'jumjiwa not found!',json_encode($sendingParams));
        }
        if (is_null($sumur)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'sumur not found!',json_encode($sendingParams));
        }
        if (is_null($sitpersil)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'sit persil not found!',json_encode($sendingParams));
        }
        if (is_null($keterangan)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'ket not found!',json_encode($sendingParams));
        }
        if (is_null($tind_lanjut)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'tindaklanjut not found!',json_encode($sendingParams));
        }
        if (is_null($tgl_realisasi)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'tgl realisasi not found!',json_encode($sendingParams));
        }
        $checkBonc=Bonc::where(['id'=>$bonc_id])->first();
        if (is_null($checkBonc)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'Bonc not found!',json_encode($sendingParams));
        }
        try{
//            $data=new Realisasi();
            $data = Realisasi::where('bonc_id',$idBonc)->first();
            $data->stndmtr=$stndmtr;
            $data->tgl_entry=$tgl_entry;
            $data->nomtr=$nomtr;
            $data->ukuranmtr=$ukuranmtr;
            $data->merkmtr=$merkmtr;
            $data->kondmtr=$kondmtr;
            $data->kondair=$kondair;
            $data->jammulai_air=$jammulai_air;
            $data->jamakhir_air=$jamakhir_air;
            $data->sglmtr=$sglmtr;
            $data->sglkopling=$sglkopling;
            $data->pipahubung=$pipahubung;
            $data->ltkmtr=$ltkmtr;
            $data->mincharge=$mincharge;
            $data->pelpgl=$pelpgl;
            $data->gnpersil=$gnpersil;
            $data->jumjiwa=$jumjiwa;
            $data->sumur=$sumur;
            $data->telpel=$telpel;
            $data->sitpersil=$sitpersil;
            $data->keterangan=$keterangan;
            $data->tind_lanjut=$tind_lanjut;
            $data->tgl_realisasi=$tgl_realisasi;
            $data->bonc_id=$bonc_id;
            if($data->status_baru==0 && $data->status_revisi==0|| $data->status_baru==5&& $data->status_revisi==0){
                $data->status_baru=1;
            }
            else if($data->status_revisi==1 && $data->status_baru==0|| $data->status_revisi==2&& $data->status_baru==0){
                $data->status_revisi=3;
            }

            if (!empty($picture)||!empty($picture2)||!empty($picture3)||!empty($picture4)) {
                if(!empty($picture)){
                    $destinationPath = 'public/uploads/foto-realisasi/';
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $filename = date("YmdHis"). '-' . $picture->getClientOriginalName();
                    $picture->move($destinationPath,$filename);
                    $data->fotometer=$filename;
                    $data->save();
                }
                if(!empty($picture2)){
                    $destinationPath = 'public/uploads/foto-realisasi/';
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $filename2 = date("YmdHis"). '-' . $picture2->getClientOriginalName();
                    $picture2->move($destinationPath,$filename2);
                    $data->fotopersil=$filename2;
                    $data->save();
                }
                if(!empty($picture3)){
                    $destinationPath = 'public/uploads/foto-realisasi/';
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $filename3 = date("YmdHis"). '-' . $picture3->getClientOriginalName();
                    $picture3->move($destinationPath,$filename3);
                    $data->fotopelanggan=$filename3;
                    $data->save();
                }
                if(!empty($picture4)){
                    $destinationPath = 'public/uploads/foto-realisasi/';
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $filename4 = date("YmdHis"). '-' . $picture4->getClientOriginalName();
                    $picture4->move($destinationPath,$filename4);
                    $data->fotottd=$filename4;
                    $data->save();
                }
            }
            elseif (empty($picture)||empty($picture2)||empty($picture3)||empty($picture4)){
                $data->save();
            }
//            $picture = $request->file('foto');
//            if (!empty($picture)) {
//                foreach ($picture as $item) {
//                    $destinationPath = 'public/uploads/foto-realisasi/';
//
//                    if (!file_exists($destinationPath)) {
//                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
//                    }
//                    $filename = date("YmdHis"). '-' . $item->getClientOriginalName();
//                    $item->move($destinationPath,$filename);
//                    $fotoRealisasi=new RealisasiFoto();
//                    $fotoRealisasi->id_realisasi=$data->id;
//                    $fotoRealisasi->foto=$filename;
//                    $fotoRealisasi->save();
//                }
//            }
            $params=[
                'code'=>302,
                'description'=>'found',
                'message'=>'Data berhasil di kirim',
                'datainput'=>$data
            ];
            return response()->json($params);

        }catch (Exception $e){
            return $this->$this->messageSystem($apiName,404,'Failed send!',json_encode($sendingParams));
        }
    }

    public function simpanHasilKontrol($idBonc,Request $request){
        $apiName='SAVE_REALISASI';
        $stndmtr=$request->stndmtr;
        $tgl_entry=$request->tgl_entry;
        $nomtr=$request->nomtr;
        $ukuranmtr=$request->ukuranmtr;
        $merkmtr=$request->merkmtr;
        $kondmtr=$request->kondmtr;
        $kondair=$request->kondair;
        $jammulai_air=$request->jammulai_air;
        $jamakhir_air=$request->jamakhir_air;
        $sglmtr=$request->sglmtr;
        $sglkopling=$request->sglkopling;
        $pipahubung=$request->pipahubung;
        $ltkmtr=$request->ltkmtr;
        $mincharge=$request->mincharge;
        $pelpgl=$request->pelpgl;
        $gnpersil=$request->gnpersil;
        $jumjiwa=$request->jumjiwa;
        $sumur=$request->sumur;
        $telpel=$request->telpel;
        $sitpersil=$request->sitpersil;
        $keterangan=$request->keterangan;
        $tind_lanjut=$request->tind_lanjut;
        $tgl_realisasi=date('Y-m-d');
        $bonc_id=$request->bonc_id;
        $picture = $request->file('fotometer');
        $picture2 = $request->file('fotopersil');
        $picture3 = $request->file('fotopelanggan');
        $picture4 = $request->file('fotottd');
        $sendingParams=[
            'stand_meter'=>$stndmtr,
            'tanggal_entry'=>$tgl_entry,
            'no_meter'=>$nomtr,
            'ukuran_meter'=>$ukuranmtr,
            'merk_meter'=>$merkmtr,
            'kondmtr'=>$kondmtr,
            'kondisi_air'=>$kondair,
            'jammulai_air'=>$jammulai_air,
            'jamakhir_air'=>$jamakhir_air,
            'segel_meter'=>$sglmtr,
            'segel_kopling'=>$sglkopling,
            'pipa_hubung'=>$pipahubung,
            'letak_meter'=>$ltkmtr,
            'minimum_charge'=>$mincharge,
            'pelanggan_panggil'=>$pelpgl,
            'guna_persil'=>$gnpersil,
            'jumlah_jiwa'=>$jumjiwa,
            'sumur'=>$sumur,
            'tlp_pelanggan'=>$telpel,
            'situasi_persil'=>$sitpersil,
            'keterangan'=>$keterangan,
            'tindak_lanjut'=>$tind_lanjut,
            'tgl_realisasi'=>$tgl_realisasi,
            'bonc_id'=>$bonc_id
        ];
        $checkBonc=Bonc::where(['id'=>$bonc_id])->first();
        if (is_null($checkBonc)){
            return $this ->messageSystem->returnApiMessage($apiName,404,'Bonc not found!',json_encode($sendingParams));
        }
        try{
//            $data=new Realisasi();
            $data = Realisasi::where('bonc_id',$idBonc)->first();
            $data->stndmtr=$stndmtr;
            $data->tgl_entry=$tgl_entry;
            $data->nomtr=$nomtr;
            $data->ukuranmtr=$ukuranmtr;
            $data->merkmtr=$merkmtr;
            $data->kondmtr=$kondmtr;
            $data->kondair=$kondair;
            $data->jammulai_air=$jammulai_air;
            $data->jamakhir_air=$jamakhir_air;
            $data->sglmtr=$sglmtr;
            $data->sglkopling=$sglkopling;
            $data->pipahubung=$pipahubung;
            $data->ltkmtr=$ltkmtr;
            $data->mincharge=$mincharge;
            $data->pelpgl=$pelpgl;
            $data->gnpersil=$gnpersil;
            $data->jumjiwa=$jumjiwa;
            $data->sumur=$sumur;
            $data->telpel=$telpel;
            $data->sitpersil=$sitpersil;
            $data->keterangan=$keterangan;
            $data->tind_lanjut=$tind_lanjut;
            $data->tgl_realisasi=$tgl_realisasi;
            $data->bonc_id=$bonc_id;
            if($data->status_baru==0 && $data->status_revisi==0){
                $data->status_baru=5;
            }
            else if($data->status_revisi==1 && $data->status_baru==0){
                $data->status_revisi=2;
            }
            $data->save();
            $data = Realisasi::where('bonc_id',$idBonc)->first();
            if (!empty($picture)||!empty($picture2)||!empty($picture3)||!empty($picture4)) {
                if(!empty($picture)){
                    $destinationPath = 'public/uploads/foto-realisasi/';
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $filename = date("YmdHis"). '-' . $picture->getClientOriginalName();
                    $picture->move($destinationPath,$filename);
                    $data->fotometer=$filename;
                    $data->save();
                }
                if(!empty($picture2)){
                    $destinationPath = 'public/uploads/foto-realisasi/';
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $filename2 = date("YmdHis"). '-' . $picture2->getClientOriginalName();
                    $picture2->move($destinationPath,$filename2);
                    $data->fotopersil=$filename2;
                    $data->save();
                }
                if(!empty($picture3)){
                    $destinationPath = 'public/uploads/foto-realisasi/';
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $filename3 = date("YmdHis"). '-' . $picture3->getClientOriginalName();
                    $picture3->move($destinationPath,$filename3);
                    $data->fotopelanggan=$filename3;
                    $data->save();
                }
                if(!empty($picture4)){
                    $destinationPath = 'public/uploads/foto-realisasi/';
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $filename4 = date("YmdHis"). '-' . $picture4->getClientOriginalName();
                    $picture4->move($destinationPath,$filename4);
                    $data->fotottd=$filename4;
                    $data->save();
                }
            }
            elseif (empty($picture)||empty($picture2)||empty($picture3)||empty($picture4)){
                $data->save();
            }
            $params=[
                'code'=>302,
                'description'=>'found',
                'message'=>'Data berhasil di kirim',
                'datainput'=>$data
            ];
            return response()->json($params);

        }catch (Exception $e){
            return $this->$this->messageSystem($apiName,404,'Failed send!',json_encode($sendingParams));
        }
    }


    public function HasilKontrol($idBonc){
        $apiName='HASIL_REALISASI';
        $getRealisasi=Realisasi::where(['bonc_id'=>$idBonc])->first();
        $sendingParams=[
            'id'=>$idBonc,
        ];
        if(is_null($getRealisasi)){
            return $this->messageSystem->returnApiMessage($apiName,404,'id BONC not found',json_encode($sendingParams));
        }
//        $dataFotoRealisasi=RealisasiFoto::where(['id_realisasi'=>$getRealisasi->id])->get();
//        foreach ($dataFotoRealisasi as $fotobonc){
//            $datafoto[]=[
//                'foto'=>'public/uploads/foto-realisasi/'.$fotobonc->foto,
//            ];
//        }

        $params=[
            'code'=>302,
            'description'=>'found',
            'message'=>'Data berhasil di dapatkan',
            'Realisasi'=>$getRealisasi,
//            'RealisasiFoto'=>$datafoto,

        ];
        return response()->json($params);
    }
}