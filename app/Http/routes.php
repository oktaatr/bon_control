<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Backend\BackendController@index')->middleware('verify-login');
Route::get('login', 'LoginController@index');
Route::get('captcha', 'LoginController@captcha');
Route::get('logout', 'LoginController@logout');
Route::post('validate-login', 'LoginController@login');
Route::group(['prefix' => 'backend', 'namespace' => 'Backend','middleware' => 'verify-login'], function () {
    Route::get('/', 'BackendController@index');
    Route::group(['prefix' => 'master', 'namespace' => 'Master','middleware' => 'verify-login'],function (){
        Route::group(['prefix' => 'bonkontrol'], function () {
            Route::get('/detail','BoncController@detail');
            Route::get('/', 'BoncController@index');
            Route::post('/add', 'BoncController@form');
            Route::post('/save', 'BoncController@save');
            Route::post('/delete', 'BoncController@delete');
        });

        Route::group(['prefix' => 'users'], function () {
            Route::get('/', 'UserController@index');
            Route::post('/add', 'UserController@form');
            Route::post('/save', 'UserController@save');
            Route::post('/delete', 'UserController@delete');
        });
        Route::group(['prefix' => 'pelanggan'], function () {
            Route::get('/', 'PelangganController@index');
            Route::post('/add', 'PelangganController@form');
            Route::post('/save', 'PelangganController@save');
            Route::post('/delete', 'PelangganController@delete');
        });
        Route::group(['prefix' => 'pemakaian-air'], function () {
            Route::get('/', 'PemakaianAirController@index');
            Route::post('/add', 'PemakaianAirController@form');
            Route::post('/save', 'PemakaianAirController@save');
            Route::post('/delete', 'PemakaianAirController@delete');
        });
        Route::group(['prefix' => 'penyebab-penyakit'], function () {
            Route::get('/', 'PenyebabPenyakitController@index');
            Route::post('/add', 'PenyebabPenyakitController@form');
            Route::post('/save', 'PenyebabPenyakitController@save');
            Route::get('/view','PenyebabPenyakitController@view');
            Route::post('/delete', 'PenyebabPenyakitController@delete');
        });
        Route::group(['prefix' => 'penyebab-klinis'], function () {
            Route::get('/', 'PenyebabKlinisController@index');
            Route::post('/add', 'PenyebabKlinisController@form');
            Route::post('/save', 'PenyebabKlinisController@save');
            Route::get('/view','PenyebabKlinisController@view');
            Route::post('/delete', 'PenyebabKlinisController@delete');
        });


    });
    Route::group(['prefix' => 'data', 'namespace' => 'Data','middleware' => 'verify-login'],function (){

        Route::group(['prefix' => 'realisasi'], function () {
            Route::get('/', 'RealisasiController@index');
            Route::post('/add-revisi', 'RealisasiController@formRevisi');
            Route::post('/add-verifikasi', 'RealisasiController@formVerif');
            Route::post('/save-revisi', 'RealisasiController@saveRevisi');
            Route::post('/save-supervisor', 'RealisasiController@saveSupervisor');
            Route::get('/view','RealisasiController@view');
            Route::post('/delete', 'RealisasiController@delete');
            Route::get('/detail','RealisasiController@detail');
            Route::post('/filter','RealisasiController@filterRealisasi');
            Route::post('/verifikasi','RealisasiController@verifikasi');

        });


    });

});


//Untuk menambah APi
Route::group(['prefix'=>'api','namespace'=>'Api'],function (){
    Route::post('/login','ApiLoginController@login');
    Route::post('/inputhasil/{id}','ApiRealisasiController@inputHasilKontrol');
    Route::get('/hasil/{id}','ApiRealisasiController@HasilKontrol');
    Route::post('/simpanhasil/{id}','ApiRealisasiController@simpanHasilKontrol');
    Route::get('/bonc-baru/{id}','ApiBoncController@getBoncBaru');
    Route::get('/bonc-proses/{id}','ApiBoncController@getBoncProses');
    Route::get('/bonc-kirim/{id}','ApiBoncController@getBoncKirim');
    Route::get('/bonc-revbaru/{id}','ApiBoncController@getBoncRevisiBaru');
    Route::get('/bonc-revproses/{id}','ApiBoncController@getBoncRevisiProses');
    Route::get('/bonc-revkirim/{id}','ApiBoncController@getBoncRevisiKirim');
    Route::get('/profile/{id}','ApiProfileController@getProfile');
    Route::get('/detailbonc/{id}','ApiBoncController@getDetailBonc');
//    Route::get('/get-realisasi/{id}','ApiRealisasiController@GetRealisasi');
    Route::get('/count-baru/{id}','ApiBoncController@countBaru');
    Route::get('/count-revisi/{id}','ApiBoncController@countRevisi');
    Route::get('/notif-baru/{id}','ApiBoncController@notifBaru');
    Route::get('/notif-revisi/{id}','ApiBoncController@notifRevisi');
    Route::post('/logout','ApiLogoutController@logout');
});