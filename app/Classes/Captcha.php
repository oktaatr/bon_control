<?php
/**
 * Created by PhpStorm.
 * User: Okta Athour Rizqi
 * Date: 27/05/2018
 * Time: 20:14
 */

namespace App\Classes;


class Captcha
{
    public function createCaptcha(){
        $characters='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength=strlen($characters);
        $randomString='';
        for($i=0;$i<6;$i++){
            $randomString.=$characters[rand(0,$charactersLength-1)];
            request()->session()->put('LoginCaptcha',$randomString);
        }
        return $randomString;
    }
}