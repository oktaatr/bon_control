<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PDAM KOTA SURABAYA</title>
    <link href="{{asset('public/assets/template/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/template/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/template/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/template/css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/corelib/ajax.css')}}">
    <style>
        body {
            background-color: white;
            background: url({{asset("public/assets/images/banner1.jpg")}});
            color: #FFF;
        }
        .halamanlogin{
            border-color:floralwhite;
            border-radius: 4px;
            height: 47px;
            /*box-shadow: 4px 2px 3px black;*/
        }
        .btnlogin{
            background-color:#0a6aa1 ;
            border: #0a6aa1;
            margin-top: 40px;
            height: 50px;
            font-size: 22px;
            font-family: "Candara";
        }

        .captcha{
            background:#116644;
            color: #fcefa1;
            font-size:30px;
        }

        form {
            color: #333;
        }
    </style>

</head>
<div class="box-tittle">
    <div class="tittleone text-center">PDAM Surya Sembada </div>
    <div class="tittlethree text-center">Kota Surabaya</div>
</div>
<div style="padding-top: 70px">
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        {{--<i class="fa fa-cart" style="font-size: 220px"></i>--}}

        <div id="results"></div>

        <form class="m-t" action="" onsubmit="return false" id="form-konten">

            <div class="form-group">
                <input type="text" name="login" class="form-control halamanlogin" placeholder="Username atau NIP" required="">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control halamanlogin" placeholder="Password" required="">
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <img id="captcha-img" src="{{url('captcha')}}" alt="Tulis angka pada gambar ini pada text box yang tersedia" style="height: 64px">
                    {{--{{$captcha}}--}}
                </div>
                <div class="col-sm-offset-7">
                    <input type="text" name="captcha" class="form-control halamanlogin" placeholder="captcha" maxlength="6" required="">
                </div>
            </div>
            <button type="submit" class="btn btn-warning block full-width m-b btnlogin">LOGIN</button>

        </form>
        <p class="m-t"> <small style="color: #0a6aa1">Controlling PDAM	&copy; 2018</small> </p>
    </div>
</div>
</div>

<!-- Mainly scripts -->
<input type='hidden' name='_token' value='{{ csrf_token() }}'>
<script src="{{asset('public/assets/template/js/jquery-2.1.1.js')}}"></script>
<script src="{{asset('public/assets/template/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/assets/corelib/core.js')}}"></script>

<script type="text/javascript">
    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        ajaxTransfer("{{url('/validate-login')}}", data, '#results');
    });

    function redirectPage(){
        redirect(1000, '/backend');
    }
</script>

</body>

</html>
