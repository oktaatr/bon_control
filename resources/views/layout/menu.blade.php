<li>
    <a href="{{url('/backend')}}"><i class="fa fa-home"></i> <span class="nav-label">Dashboard</span> </a>
</li>
<li>
    <a href="#"><i class="fa fa-files-o"></i> <span class="nav-label">Master</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{url('/backend/master/pelanggan')}}">Pelanggan</a></li>
        <li><a href="{{url('/backend/master/bonkontrol')}}">Bonc</a></li>
        <li><a href="{{url('/backend/master/pemakaian-air')}}">Pemakaian Air</a></li>
        <li><a href="{{url('/backend/master/users')}}">User</a></li>
    </ul>
</li>

<li>
    <a href="{{url('/backend/data/realisasi')}}"><i class="fa fa-send"></i> <span class="nav-label">Status Bon C</span> </a>
</li>

<li>
    <a href="{{url('/logout')}}"><i class="fa fa-sign-out"></i> <span class="nav-label">Logout</span></a>
</li>