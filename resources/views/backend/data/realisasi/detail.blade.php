@extends('layout.main')
@section('title', $title)
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Detail Status BonControl</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">Home</a>
                </li>

                <li>
                    <a href="#">Master</a>
                </li>


                <li class="active">
                    <a>Status Bon Control</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>


    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        @if($dataRealisasi->status_baru==1)
                            <a class="btn btn-primary" title="Tambah Data">Sudah Realisasi</a>
                        @endif
                        @if($dataRealisasi->status_baru==2)
                                <a class="btn btn-success" title="Tambah Data">Verifikasi</a>
                            @endif
                            @if($dataRealisasi->status_baru==3)
                                <a class="btn btn-success" title="Tambah Data">Verifikasi Supervisor</a>
                            @endif
                            @if($dataRealisasi->status_revisi==1)
                                <a class="btn btn-danger" title="Tambah Data">Revisi</a>
                            @endif
                        <div class="table-responsive">
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table id="table-role" class="table table-striped table-bordered table-hover">
                                        <tr>
                                            <td width="30%">NOMOR BONC</td>
                                            <td>{{$dataBonc->no_bonc}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Tgl Serah Bon</td>
                                            <td>{{$dataRealisasi->tgl_realisasi}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">BAGIAN PEMAKAIAN AIR </td>
                                            <td>{{$dataBonc->bagpemair}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Tanggal BONC</td>
                                            <td>{{$dataBonc->tglbonc}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Sumber Data</td>
                                            <td>{{$dataBonc->sbrdt}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Asal Adu</td>
                                            <td>{{$dataBonc->asal_adu}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Tlp</td>
                                            <td>{{$dataBonc->tlp}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Permasalahan</td>
                                            <td>{{$dataBonc->masalah}}</td>
                                        </tr>

                                        <tr>
                                            <td width="30%">No Pelanggan</td>
                                            <td>{{$dataBonc->getPelanggan->nopel}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Nama</td>
                                            <td>{{$dataBonc->getPelanggan->nama_pel}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Alamat</td>
                                            <td>{{$dataBonc->getPelanggan->alamat}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Retribusi</td>
                                            <td>{{$dataBonc->getPelanggan->retrib}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">No PA</td>
                                            <td>{{$dataBonc->getPelanggan->nopa}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Kode Tarip</td>
                                            <td>{{$dataBonc->getPelanggan->kdtrp}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Data meter</td>
                                            <td>{{$dataBonc->getPelanggan->dtmtr}}</td>
                                        </tr>
                                    </table>
                                    {{--<table id="table-role" class="table table-striped table-bordered table-hover">--}}
                                        {{--<thead>--}}
                                        {{--<tr>--}}
                                            {{--<th>Tgl Catat</th>--}}
                                            {{--<th>MT Lalu</th>--}}
                                            {{--<th>MT Kini</th>--}}
                                            {{--<th>M Air</th>--}}
                                        {{--</tr>--}}
                                        {{--</thead>--}}
                                        {{--<tbody>--}}
                                        {{--@foreach($datapemair as $num => $item)--}}
                                        {{--<tr>--}}
                                            {{--<td>{{$item->tglcatat}}</td>--}}
                                            {{--<td>{{$item->mtlalu}}</td>--}}
                                            {{--<td>{{$item->mtkini}}</td>--}}
                                            {{--<td>{{$item->mair}}</td>--}}
                                        {{--</tr>--}}
                                        {{--@endforeach--}}
                                        {{--</tbody>--}}

                                    {{--</table>--}}
                                    <table id="table-role" class="table table-striped table-bordered table-hover">
                                        <tr>
                                            <td width="30%">Stand meter</td>
                                            <td>{{$dataRealisasi->stndmtr}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Tanggal</td>
                                            <td>{{$dataRealisasi->tgl_entry}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">No Meter</td>
                                            <td>{{$dataRealisasi->nomtr}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Ukuran pipa</td>
                                            <td>{{$dataRealisasi->ukuranmtr}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Merk meter</td>
                                            <td>{{$dataRealisasi->merkmtr}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Kondisi Meter</td>
                                            <td>{{$dataRealisasi->kondmtr}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Kondisi Air</td>
                                            <td>{{$dataRealisasi->kondair}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Jam Aliran Air</td>
                                            <td>{{$dataRealisasi->jammulai_air}} WIB.s/d {{$dataRealisasi->jamakhir_air}}</td>
                                        </tr>

                                        <tr>
                                            <td width="30%">Segel Mtr</td>
                                            <td>{{$dataRealisasi->sglmtr}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Segel Kopling</td>
                                            <td>{{$dataRealisasi->sglkopling}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Pipa Penghubung</td>
                                            <td>{{$dataRealisasi->pipahubung}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Letak Meter</td>
                                            <td>{{$dataRealisasi->ltkmtr}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Minimum Charge</td>
                                            <td>{{$dataRealisasi->mincharge}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Pelanggan panggil</td>
                                            <td>{{$dataRealisasi->pelpgl}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Guna persil</td>
                                            <td>{{$dataRealisasi->gnpersil}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Jumlah jiwa</td>
                                            <td>{{$dataRealisasi->jumjiwa}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Sumur</td>
                                            <td>{{$dataRealisasi->sumur}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Telp Pelanggan</td>
                                            <td>{{$dataRealisasi->telpel}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Situasi Persil</td>
                                            <td>{{$dataRealisasi->sitpersil}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Keterangan</td>
                                            <td>{{$dataRealisasi->keterangan}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Tindak Lanjut</td>
                                            <td>{{$dataRealisasi->tind_lanjut}}</td>
                                        </tr>
                                    </table>
                                    <div class="box-body">
                                        <strong><i class="fa fa-book margin-r-5"></i>  Foto meter </strong>
                                        <a class="fancybox" rel="gallery1" href="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->fotometer)}}" title="Vertical - Special Edition! (cedarsphoto)">
                                            <img src="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->fotometer)}}" alt="Foto Belum terupload" />
                                        </a>
                                        <hr>
                                        <strong><i class="fa fa-book margin-r-5"></i>  Foto persil </strong>
                                        <a class="fancybox" rel="gallery1" href="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->fotopersil)}}" title="Vertical - Special Edition! (cedarsphoto)">
                                            <img src="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->fotopersil)}}" alt="Foto Belum terupload" />
                                        </a>
                                        <hr>
                                        <strong><i class="fa fa-book margin-r-5"></i>  Foto pelanggan </strong>
                                        <a class="fancybox" rel="gallery1" href="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->fotopelanggan)}}" title="Vertical - Special Edition! (cedarsphoto)">
                                            <img src="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->fotopelanggan)}}" alt="Foto Belum terupload" />
                                        </a>
                                        <hr>
                                        <strong><i class="fa fa-book margin-r-5"></i>  Foto Rumah</strong>
                                        <a class="fancybox" rel="gallery1" href="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->fotottd)}}" title="Vertical - Special Edition! (cedarsphoto)">
                                            <img src="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->fotottd)}}" alt="Foto Belum terupload" />
                                        </a>
                                    </div><!-- /.box-body -->
                                    <center>
                                        @if($dataRealisasi->status_baru==1 || $dataRealisasi->status_revisi==1 || $dataRealisasi->status_revisi==3)
                                            <a style="margin-top: 20px;" onclick="loadModal(this)" target="/backend/data/realisasi/add-revisi" data="id={{$dataRealisasi->id}}"
                                               class="btn btn-danger" title="Revisi">Revisi</a>
                                        @endif
                                        @if(Session::get('activeUser')->role_id==1 && $dataRealisasi->status_baru==1 || $dataRealisasi->status_revisi==3)
                                                <a style="margin-top: 20px;" onclick="loadModal(this)" target="/backend/data/realisasi/add-verifikasi" data="id={{$dataRealisasi->id}}"
                                                   class="btn btn-primary" title="Verifikasi">Verifikasi</a>
                                            @endif
                                        @if(Session::get('activeUser')->role_id==2 && $dataRealisasi->status_baru==4)
                                                <a style="margin-top: 20px;" onclick="loadModal(this)" target="/backend/data/realisasi/add-verifikasi" data="id={{$dataRealisasi->id}}"
                                                   class="btn btn-success" title="Verifikasi">Verifikasi</a>
                                            @endif
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('scripts')
    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="{{asset('public/assets/fancybox/lib/jquery.mousewheel.pack.js')}}"></script>

    <!-- Add fancyBox -->
    <script type="text/javascript" src="{{asset('public/assets/fancybox/source/jquery.fancybox.pack.js')}}"></script>

    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <script type="text/javascript" src="{{asset('public/assets/fancybox/source/helpers/jquery.fancybox-buttons.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/assets/fancybox/source/helpers/jquery.fancybox-media.js')}}"></script>

    <script type="text/javascript" src="{{asset('public/assets/fancybox/source/helpers/jquery.fancybox-thumbs.js')}}"></script>
    <script>
        // var table = $('#table-role');
        // table.dataTable({
        //     pageLength: 10,
        //     responsive: true,
        //     dom: '<"html5buttons"B>lTfgitp',
        //     columnDefs: [
        //         {"targets": 0, "orderable": false},
        //         // {"targets": 1, "visible": false, "searchable": false},
        //     ],
        //     order: [[0, "asc"]],
        //     buttons: [
        //         {extend: 'copy'},
        //         {extend: 'csv', title: 'Tipe Fasilitas'},
        //         {extend: 'excel', title: 'Tipe Fasilitas'},
        //         {extend: 'pdf', title: 'Tipe Fasilitas'},
        //         {
        //             extend: 'print',
        //             customize: function (win) {
        //                 $(win.document.body).addClass('white-bg');
        //                 $(win.document.body).css('font-size', '10px');
        //
        //                 $(win.document.body).find('table')
        //                     .addClass('compact')
        //                     .css('font-size', 'inherit');
        //             }
        //         }
        //     ]
        // });
        $(document).ready(function() {
            $(".fancybox").fancybox({
                openEffect	: 'none',
                closeEffect	: 'none'
            });
        });
    </script>
@endsection

@endsection