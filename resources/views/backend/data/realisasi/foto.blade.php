<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="text/javascript" src="{{asset("public/assets/photo/jquery.js")}}" ></script>
    <script type="text/javascript" src="{{asset("public/assets/photo/jqueryui.js")}}" ></script>
    <script type="text/javascript" src="{{asset("public/assets/photo/jquery.mousewheel.js")}}" ></script>
    <script type="text/javascript" src="{{asset("public/assets/photo/jquery.iviewer.js")}}" ></script>
    <script type="text/javascript">
        var $ = jQuery;
        $(document).ready(function(){
            var iv1 = $("#viewer").iviewer({
                src: "{{$src}}",
                update_on_resize: false,
                zoom_animation: false,
                mousewheel: false,
                onMouseMove: function(ev, coords) { },
                onStartDrag: function(ev, coords) { return false; }, //this image will not be dragged
                onDrag: function(ev, coords) { }
            });

            $("#in").click(function(){ iv1.iviewer('zoom_by', 1); });
            $("#out").click(function(){ iv1.iviewer('zoom_by', -1); });
            $("#fit").click(function(){ iv1.iviewer('fit'); });
            $("#orig").click(function(){ iv1.iviewer('set_zoom', 100); });
            $("#update").click(function(){ iv1.iviewer('update_container_info'); });

            var iv2 = $("#viewer2").iviewer(
                {
                    src: "{{$src}}"
                });

            $("#chimg").click(function()
            {
                iv2.iviewer('loadImage', "{{$src}}");
                return false;
            });

            var fill = false;
            $("#fill").click(function()
            {
                fill = !fill;
                iv2.iviewer('fill_container', fill);
                return false;
            });
        });
    </script>
    <link rel="stylesheet" href="{{asset("public/assets/photo/jquery.iviewer.css")}}" />
    <style>
        .viewer
        {
            width: 100%;
            height: 500px;
            border: 1px solid black;
            position: relative;
        }

        .wrapper
        {
            overflow: hidden;
        }
    </style>
</head>
<body>

<div class="wrapper">

    <div id="viewer2" class="viewer" style="width: 100%;"></div>


</div>
</body>
</html>
