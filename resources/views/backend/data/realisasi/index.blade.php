@extends('layout.main')
@section('title', $title)
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data Realisasi</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">Home</a>
                </li>

                <li>
                    <a href="#">Data</a>
                </li>


                <li class="active">
                    <a>Realisasi</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>


    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        {{--<a onclick="loadModal(this)" target="/backend/master/bonkontrol/add" class="btn btn-primary" title="Tambah Data"><i--}}
                                    {{--class="glyphicon glyphicon-plus"></i> Tambah Data</a>--}}
                        <div class="table-responsive">
                            <div class="dataTables_ex">
                            <select name="filter-realisasi" class="form-control" id="idfilter" onchange="postDataTabe()">
                                <option value="all">Pilih Status Bon Control</option>
                                <option value="new">Baru</option>
                                <option value=1>Realisasi</option>
                                <option value=2>Verifikasi</option>
                                <option value=3>Disposisi Supervisor</option>
                                <option value=4>Proses Baru</option>
                                <option value=5>Pending Supervisor</option>
                                <option value=6>Revisi</option>
                                <option value=7>Proses Revisi</option>
                                <option value=8>Realisasi Revisi</option>
                            </select>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="table-role">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Status</th>
                                            <th>Tgl</th>
                                            <th>Bonc</th>
                                            <th>Petugas</th>
                                            <th>Nomer Pelanggan</th>
                                            <th>Nama Pelanggan</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        {{--<tbody>--}}
                                        {{--@foreach($data as $num => $item)--}}
                                            {{--<tr>--}}
                                                {{--<td>{{$num+1}}</td>--}}
                                                {{--<td><center>--}}
                                                        {{--@if($item->status_revisi==1)--}}
                                                            {{--<a class="btn btn-danger">REVISI</a>--}}
                                                        {{--@endif--}}
                                                        {{--@if($item->status_baru==1)--}}
                                                            {{--<a class="btn btn-primary">SUDAH REALISASI</a>--}}
                                                        {{--@endif--}}
                                                        {{--@if($item->status_baru==2)--}}
                                                            {{--<a class="btn btn-success">Verifikasi</a>--}}
                                                        {{--@endif--}}
                                                        {{--@if($item->status_baru==3)--}}
                                                            {{--<a class="btn btn-success">Verifikasi Supervisor</a>--}}
                                                        {{--@endif--}}
                                                    {{--</center>--}}
                                                {{--</td>--}}
                                                {{--<td>{{$item->tgl_realisasi}}</td>--}}
                                                {{--<td>{{$item->getDatabon->no_bonc}}</td>--}}
                                                {{--<td>{{$item->getDatabon->getPelanggan->nopel}}</td>--}}
                                                {{--<td>{{$item->getDatabon->getPelanggan->nama}}</td>--}}
                                                {{--<td>{{$item->getDatabon->getPetugas->nama}}</td>--}}
                                                {{--<td>--}}
                                                    {{--<a href="{{url('backend/data/realisasi/detail?id=')}}{{$item->id}}" class="btn btn-primary btn-s">Detail</a>--}}
                                                    {{--<a onclick="deleteData({{$item->id}})" class="btn btn-danger btn-s glyphicon glyphicon-trash"--}}
                                                       {{--title="Hapus Data"></a>--}}
                                                {{--</td>--}}
                                            {{--</tr>--}}
                                        {{--@endforeach--}}
                                        {{--</tbody>--}}
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('customscripts')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apakah anda yakin akan menghapus data?", function () {
                ajaxTransfer("/backend/master/bonkontrol/delete", data, "#modal-output");
            })
        }
        function detailData(id) {
            var data=new FormData();
            data.append('id',id);
            ajaxTransfer("backend/data/realisasi/detail",data,"#table-role");
        }

        {{--var table = $('#table-role');--}}
        {{--table.dataTable({--}}
            {{--processing: true,--}}
            {{--serverSide: true,--}}
            {{--ajax:"{{ url('/backend/data/filter-realisasi') }}",--}}
            {{--columns: [--}}
                {{--{"data":"id"},--}}
                {{--{"data":"tgl_entry"},--}}
                {{--{"data":"stndmtr"},--}}
                {{--{"data":"bonc_id"},--}}
                {{--{"data":"status_baru"},--}}
                {{--{"data":"status_revisi"},--}}
                {{--{"data":"tgl_realisasi"},--}}
            {{--],--}}
            {{--pageLength: 10,--}}
            {{--responsive: true,--}}
            {{--dom: '<"html5buttons"B>lTfgitp',--}}
            {{--columnDefs: [--}}
                {{--{"targets": 0, "orderable": false},--}}
                {{--// {"targets": 1, "visible": false, "searchable": false},--}}
            {{--],--}}
            {{--order: [[0, "asc"]],--}}
            {{--buttons: [--}}
                {{--{extend: 'copy'},--}}
                {{--{extend: 'csv', title: 'Tipe Fasilitas'},--}}
                {{--{extend: 'excel', title: 'Tipe Fasilitas'},--}}
                {{--{extend: 'pdf', title: 'Tipe Fasilitas'},--}}
                {{--{--}}
                    {{--extend: 'print',--}}
                    {{--customize: function (win) {--}}
                        {{--$(win.document.body).addClass('white-bg');--}}
                        {{--$(win.document.body).css('font-size', '10px');--}}

                        {{--$(win.document.body).find('table')--}}
                            {{--.addClass('compact')--}}
                            {{--.css('font-size', 'inherit');--}}
                    {{--}--}}
                {{--}--}}
            {{--]--}}
        {{--});--}}

            {{--$("#idfilter").on('change',function(){--}}
                {{--var filter = $(this).val();--}}
                {{--$('#table-role').DataTable({--}}
                    {{--processing: true,--}}
                    {{--serverSide: true,--}}
                    {{--ajax:"{{ url('/backend/data/filter-realisasi') }}",--}}
                    {{--columns: [--}}
                        {{--{"data":"id"},--}}
                        {{--{"data":"tgl_entry"},--}}
                        {{--{"data":"stndmtr"},--}}
                        {{--{"data":"bonc_id"},--}}
                        {{--{"data":"status_baru"},--}}
                        {{--{"data":"status_revisi"},--}}
                        {{--{"data":"tgl_realisasi"},--}}
                        {{--],--}}
                {{--});--}}
             {{--});--}}

            {{--$("#idfilter").on('change',function(){--}}
                {{--var filter = $(this).val();--}}
                {{--$.ajax({--}}
                    {{--'type': 'get',--}}
                    {{--dataType: 'json',--}}
                    {{--url: '{{url('/backend/data/realisasi/filter-realisasi')}}',--}}
                    {{--data: {'status_id':+filter},--}}
                    {{--success:function(response){--}}
                        {{--console.log(response);--}}
                        {{--$("#table-role").dataTable();--}}
                    {{--}--}}
                {{--});--}}
            {{--});--}}

        // $(document).ready(function () {
            var dataTableRealisasi=$('#table-role').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{ url('/backend/data/realisasi/filter') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {
                        _token: "{{csrf_token()}}",
                    }
                },
                "columns": [
                    {"data": "id"},
                    {"data": "status"},
                    {"data": "tgl"},
                    {"data": "bonc_id"},
                    {"data": "nama_petugas"},
                    {"data": "nopel"},
                    {"data": "pelanggan"},
                    {"data": "aksi"}
                ],
                "pageLength": 10,
                "responsive":true,
                "columnDefs": [
                    {"targets": 0, "orderable": false},
                    // {"targets": 1, "visible": false, "searchable": false},
                ],
                dom: '<"html5buttons"B>lTfgitp',
                order: [[1, "asc"]],
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv', title: 'Tipe Fasilitas'},
                    {extend: 'excel', title: 'Tipe Fasilitas'},
                    {extend: 'pdf', title: 'Tipe Fasilitas'},
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });
            $('#idfilter').on('change', function(){
                var filter_value = $(this).val();
                var new_url ="{{url('/backend/data/realisasi/filter?status=')}}"+filter_value;
                dataTableRealisasi.ajax.url(new_url).load();
            });
        // })

    </script>
@endsection

@endsection