<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>

    @if(Session::get('activeUser')->role_id==1||Session::get('activeUser')->role_id==1 && $data->status_revisi==1)
    <div class="form-group">
        <label class="col-lg-2 control-label">Status</label>
        <div class="col-lg-10">
            <input type="radio" name="status_baru" value="2"
                   > Verifikasi

            <input type="radio" name="status_baru" value="4"
                   > Verifikasi Supervisor
        </div>
    </div>
    @endif


    @if(Session::get('activeUser')->role_id==2 ||Session::get('activeUser')->role_id==2 && $data->status_revisi==1)
    <div class='form-group'>
        <label for='ket_supervisor' class='col-sm-2 col-xs-12 control-label'>Keterangan</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="ket_supervisor" class="form-control" value="" required="">
        </div>
    </div>
    @endif
    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    @if($data->status_baru==4)
    //verifikasi supervisor
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/data/realisasi/save-supervisor', data, '#result-form-konten');
        })
    })
    @endif
    @if($data->status_baru==1 || $data->status_revisi==3)
    //verifikasi admin
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/data/realisasi/verifikasi', data, '#result-form-konten');
        })
    })
    @endif
</script>
