<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>

    <div class='form-group'>
        <label for='no_bonc' class='col-sm-2 col-xs-12 control-label'>No Bonc</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="no_bonc" class="form-control" value="{{$data->no_bonc}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='tglbonc' class='col-sm-2 col-xs-12 control-label'>tgl Bon</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="date" name="tglbonc" class="form-control" value="{{$data->tglbonc}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='bagpemair' class='col-sm-2 col-xs-12 control-label'>Bag Pem Air</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="bagpemair" class="form-control" value="{{$data->bagpemair}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='sbrdt' class='col-sm-2 col-xs-12 control-label'>Sumber Data</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="sbrdt" class="form-control" value="{{$data->sbrdt}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='asal_adu' class='col-sm-2 col-xs-12 control-label'>Asal Adu</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="asal_adu" class="form-control" value="{{$data->asal_adu}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='tlp' class='col-sm-2 col-xs-12 control-label'>Tlp</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="tlp" class="form-control" value="{{$data->tlp}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='masalah' class='col-sm-2 col-xs-12 control-label'>Masalah</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="masalah" class="form-control" value="{{$data->masalah}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='id_pel' class='col-sm-2 col-xs-12 control-label'>Pelanggan</label>
        <div class='col-sm-9 col-xs-12'>
            <select name="id_pel" class="form-control">
                @foreach($pelangganOption as $num => $item)
                    <option value="{{$item->id}}"
                            @if(!is_null($data))
                            @if($item->id == $data->id_pel) selected="selected"
                            @endif
                            @endif >{{$item->nama_pel}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class='form-group'>
        <label for='id_user' class='col-sm-2 col-xs-12 control-label'>Petugas</label>
        <div class='col-sm-9 col-xs-12'>
            <select name="id_user" class="form-control">
                @foreach($userOption as $num => $item)
                    <option value="{{$item->id}}" @if(!is_null($data)) @if($item->id == $data->id_user) selected="selected" @endif @endif >{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/bonkontrol/save', data, '#result-form-konten');
        })
    })
</script>
