@extends('layout.main')
@section('title', $title)
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Detail BonControl</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">Home</a>
                </li>

                <li>
                    <a href="#">Master</a>
                </li>


                <li class="active">
                    <a>Detail Bon Control</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>


    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table id="table-role" class="table table-striped table-bordered table-hover">
                                        <tr>
                                            <td width="30%">Nomor Bonc</td>
                                            <td>{{$dataBonc->no_bonc}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">tgl Bon</td>
                                            <td>{{date('d-m-Y',strtotime($dataBonc->tglbonc))}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Bagian pemakaian air</td>
                                            <td>{{$dataBonc->bagpemair}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Sumber data</td>
                                            <td>{{$dataBonc->sbrdt}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Asal adu</td>
                                            <td>{{$dataBonc->asal_adu}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Tlp</td>
                                            <td>{{$dataBonc->tlp}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Permasalahan</td>
                                            <td>{{$dataBonc->masalah}}</td>
                                        </tr>

                                    </table>
                                    <table id="table-role" class="table table-striped table-bordered table-hover">
                                        <tr>
                                            <td width="30%">Nomor Pelanggan</td>
                                            <td>{{$dataBonc->getPelanggan->nopel}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Zona</td>
                                            <td>{{$dataBonc->getPelanggan->zone}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Nama</td>
                                            <td>{{$dataBonc->getPelanggan->nama_pel}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Alamat</td>
                                            <td>{{$dataBonc->getPelanggan->alamat}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Retribusi</td>
                                            <td>{{$dataBonc->getPelanggan->retrib}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Nomer P.A</td>
                                            <td>{{$dataBonc->getPelanggan->nopa}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Kode Tarip</td>
                                            <td>{{$dataBonc->getPelanggan->kdtrp}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Data meter</td>
                                            <td>{{$dataBonc->getPelanggan->dtmtr}}</td>
                                        </tr>

                                    </table>
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>Tgl Catat</th>
                                            <th>MT Lalu</th>
                                            <th>MT kini</th>
                                            <th>MT Air</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($datapemair as $num => $item)
                                            <tr>
                                                <td>{{date('d-m-Y',strtotime($item->tglcatat))}}</td>
                                                <td>{{$item->mtlalu}}</td>
                                                <td>{{$item->mtkini}}</td>
                                                <td>{{$item->mair}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('scripts')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apakah anda yakin akan menghapus data?", function () {
                ajaxTransfer("/backend/master/pemakain-air/delete", data, "#modal-output");
            })
        }

        var table = $('#table-role');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[0, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection