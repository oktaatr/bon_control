<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>

    <div class='form-group'>
        <label for='tglcatat' class='col-sm-2 col-xs-12 control-label'>Tgl Catat</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="date" name="tglcatat" class="form-control" value="{{$data->tglcatat}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='mtlalu' class='col-sm-2 col-xs-12 control-label'>MT Lalu</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="mtlalu" class="form-control" value="{{$data->mtlalu}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='mtkini' class='col-sm-2 col-xs-12 control-label'>MT Kini</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="mtkini" class="form-control" value="{{$data->mtkini}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='idpel' class='col-sm-2 col-xs-12 control-label'>Nama Pelanggan</label>
        <div class='col-sm-9 col-xs-12'>
            <select name="idpel" class="form-control">
                @foreach($pelOption as $num => $item)
                    <option value="{{$item->id}}" @if(!is_null($data)) @if($item->id == $data->idpel) selected="selected" @endif @endif >{{$item->nama_pel}} ({{$item->nopel}})</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/pemakaian-air/save', data, '#result-form-konten');
        })
    })
</script>
