<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>

    <div class='form-group'>
        <label for='nopel' class='col-sm-2 col-xs-12 control-label'>No Pelanggan</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="nopel" class="form-control" value="{{$data->nopel}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='zone' class='col-sm-2 col-xs-12 control-label'>Zona</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="zone" class="form-control" value="{{$data->zone}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama_pel' class='col-sm-2 col-xs-12 control-label'>Nama</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="nama_pel" class="form-control" value="{{$data->nama_pel}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='alamat' class='col-sm-2 col-xs-12 control-label'>Alamat</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="alamat" class="form-control" value="{{$data->alamat}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='retrib' class='col-sm-2 col-xs-12 control-label'>Retribusi</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="retrib" class="form-control" value="{{$data->retrib}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='kdtrp' class='col-sm-2 col-xs-12 control-label'>Kode tarip</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="kdtrp" class="form-control" value="{{$data->kdtrp}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nopa' class='col-sm-2 col-xs-12 control-label'>No PA</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="nopa" class="form-control" value="{{$data->nopa}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='dtmtr' class='col-sm-2 col-xs-12 control-label'>Data meter</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="dtmtr" class="form-control" value="{{$data->dtmtr}}" required="">
        </div>
    </div>

    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/pelanggan/save', data, '#result-form-konten');
        })
    })
</script>
